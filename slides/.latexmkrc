# Minted
&set_tex_cmds( '-shell-escape %O '
        . '\'\PassOptionsToPackage{outputdir={%Y}}{minted}\input{%S}\''
        );

$pdflatex = 'lualatex -shell-escape -file-line-error -interaction=nonstopmode -synctex=1 -output-directory=build %O %S | texlogsieve';
$aux_dir = 'build';
$bibtex_use = 2;
# Amend cleaned extensions
$clean_ext .= " fdb_latexmk run.xml synctex.gz";
# Make latexmk quiet
$latexmk_silent = 1;

# Makeglossaries
add_cus_dep('acn', 'acr', 0, 'makeglossaries');
add_cus_dep('glo', 'gls', 0, 'makeglossaries');
$clean_ext .= " acr acn alg glo gls glg";

sub makeglossaries {
    my ($base_name, $path) = fileparse( $_[0] );
    my @args = ( "-q", "-d", $path, $base_name );
    if ($silent) { unshift @args, "-q"; }
    return system "makeglossaries", "-d", $path, $base_name;
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Slides - Internship defense - CRNL
%
% Lyon Neurosciences Research Center
% May 15th to July 7th 2023
%
% L3 - Informatique - Sciences de la vie
%
% Version: v0.0.1
% Author: Samuel ORTION
% Date: 2023-07-03
% License: CC By-SA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{beamer}

\usepackage{crnl}

\title{Analysis of rat electrophysiological data}
\subtitle{L3 internship defense}
\author{Samuel Ortion}
\date{July 3\textsuperscript{rd} 2023}
\institute{Lyon Neuroscience Research Center \\[1em]
Licence Double Informatique -- Sciences de la vie \\
University of Évry -- Paris-Saclay University
}
\titlegraphic{
    \begin{picture}(0,0)
        \put(305,-120){\makebox(0,0)[rt]{\includegraphics[height=2cm]{../media/logo/logo-crnl.png}}}
    \end{picture}
}

\makeatletter
\hypersetup{
    pdfauthor={\@author},
    pdftitle={\@title},
    pdfsubject={Internship defense},
    pdfkeywords={L3, internship, python, neuroscience, rat, olfaction, electrophysiology, LFP, respiration, whisking}
}
\makeatother

\addbibresource{../references.bib}

\begin{document}

\begin{frame}[noframenumbering,plain]
    \maketitle
\end{frame}

% \begin{frame}{Outline}
%     \tableofcontents
% \end{frame}

\section{Introduction}

\begin{frame}{Olfaction in rodents}

    \begin{columns}
        \begin{column}{0.4\columnwidth}
            \vfill
            \scalebox{-1}[1]{\includegraphics[width=\textwidth]{../media/pictures/long-evans-3a.pdf}}
        \end{column}
        \begin{column}{0.6\columnwidth}
            \begin{itemize}
                \item Behaviour:  Social interactions, Food finding \dots
                \item Used for learning and memory tasks in rodents.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Olfactory Experiment}

\begin{frame}{Olfactory Discrimination Task}
    \begin{columns}
        \begin{column}{0.5\columnwidth}
            \vfill
            \includegraphics[width=15em]{../media/pictures/experiment_scheme.png}
            \vfill
        \end{column}
        \begin{column}{0.5\columnwidth}
            \begin{itemize}
                \item Two odors, each associated with a water port;
                \item Odor release on nose poke (from center odor port);
                \item Reward if rat headed first to the correct lateral port within 6~s. \\
                      \cite{lefevreSignificanceSniffingPattern2016}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Experiments timeline}
    \begin{figure}
        \centering
        \includegraphics[width=0.75\textwidth]{../media/pictures/timeline_nodor.png}
        \caption{Timeline of the experiment phases.}
    \end{figure}
    \begin{itemize}
        \item Naive rats in P1-S1;
        \item LC1: Level Criterion ($\geq 80\%$ correct trials);
        \item 100-200 trials per test day.
    \end{itemize}

\end{frame}

\subsection{Brain Rhythms}

\begin{frame}{Electrophysiological Recordings -- Local Field Potential (LFP) Principles}
    \includegraphics[width=15em]{../media/pictures/lfp/lfp.pdf}
    From \cite{jiaGammaRhythmsBrain2011}.
\end{frame}


\begin{frame}{Electrophysiological Recordings -- Brain Areas}
    \begin{columns}
        \begin{column}{0.45\columnwidth}
            \includegraphics[height=\textwidth]{../media/pictures/rat_brain_structures.pdf}
        \end{column}
        \begin{column}{0.45\columnwidth}
            \begin{itemize}
                \item \textbf{Cereb}: Cerebellum
                \item \textbf{Hipp}: Hippocampus
                \item \textbf{Stri}: Striatum
                \item \textbf{OB}: Olfactory Bulb
                \item \textbf{OT}: Olfactory Tubercle
                \item \textbf{AP}: Anterior Piriform cortex
                \item \textbf{PP}: Posterior Piriform cortex
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Recording Respiratory Signal with a Plethysmograph}
    \begin{figure}
        \centering
        \includegraphics[width=0.25\textwidth]{../media/pictures/whole-body-plethysmograph.png}
        \caption{A whole-body plethysmograph allowing to record respiratory signal \cite{hegoburuRUBCageRespirationUltrasonic2011}.}
    \end{figure}
\end{frame}

\begin{frame}{An example of recorded signals}
    \includegraphics[height=\textheight]{../media/plots/signal_zoom_extract_Rtbsi28_P1_LC2_annotated_2.pdf}
\end{frame}

\begin{frame}{Objectives}
    \begin{itemize}
        \item How do brain rhythms evolve during an olfactory task learning in rats?
              \begin{enumerate}
                  \item How is slow rhythms phase constrained by the experiments?
                  \item How is oscillations amplitude modulated?
                  \item How does brain areas functional connectivity evolve?
              \end{enumerate}
    \end{itemize}
\end{frame}


\section{Analyses}

\subsection{Phase Reset}

\begin{frame}{Phase Reset -- Methods}
    \begin{figure}
        \centering
        \includegraphics[width=0.75\textwidth]{../media/plots/lfp_signal_filtering_lefevre.pdf}
    \end{figure}
    \begin{enumerate}
        \item Band pass filtering (e.g 5--15~Hz);
        \item Phases from Hilbert transform,

              aligned to a time reference trial by trial;
        \item Phase synchrony assessed by Rayleigh test.
    \end{enumerate}
\end{frame}

\begin{frame}{Phase Reset -- Results}
    \begin{figure}
        \centering
        \includegraphics[width=0.65\textwidth]{../media/plots/rayleigh_zscore_sniff_Rtbsi11_1-15Hz.pdf}
        \caption{Phase reset of sniffing rhythm for rat \texttt{Rtbsi11} on nose poke.}
    \end{figure}

    % A colored box
    \begin{itemize}
        \item Rats enter the odor port with a breath;
        \item No phase reset on hippocampal $\theta$ rhythm (at nose poke).
    \end{itemize}
\end{frame}

\subsection{Amplitude Modulation}

\begin{frame}{Amplitude Modulation -- Methods}
    \begin{figure}
        \centering
        \includegraphics[width=0.5\textwidth]{../media/plots/tfmaps_gamma_measure_zone.png}
        \caption{Averaged time-frequency map of LFP signal}
    \end{figure}

    \begin{enumerate}
        \item Standard Morlet wavelet transform (time-frequency map per trial);
        \item Measure of 3\textsuperscript{rd} quartile of amplitude distribution in a time-frequency window normalized against the median in a reference zone.
    \end{enumerate}

\end{frame}

\begin{frame}{Amplitude Modulation -- Results}
    \begin{figure}
        \centering
        \begin{subfigure}{0.45\textwidth}
            \includegraphics[height=12em]{../media/plots/beta_amplitude_modulation_ob.pdf}
            \caption{Beta amplitude modulation}
        \end{subfigure}
        \begin{subfigure}{0.5\textwidth}
            \includegraphics[height=12em]{../media/plots/rat_performance.pdf}
            \caption{Rat performance}
        \end{subfigure}
        \caption{$\beta$ amplitude modulation follows the same pattern as rat performance (20--30~Hz, -400-100~ms at port exit).}
    \end{figure}
    \cite{fourcaud-trocmeHighBetaRhythm2019}
\end{frame}

\subsection{Brain Areas Connectivity}

\begin{frame}{Brain Areas Connectivity (WPLI) -- Methods}
    \begin{figure}
        \centering
        \includegraphics[width=0.65\textwidth]{../media/plots/WPLI_tfmap_BO-PP_Rtbsi25_22.png}
        \caption{Weighted Phase Lag Index (WPLI) between OB and PP for rat \texttt{Rtbsi25} at nose poke.}
    \end{figure}

    \begin{enumerate}
        \item Standard wavelet transform spectra;
        \item WPLI between brain areas for each time-frequency bin \cite{vinckImprovedIndexPhasesynchronization2011};
        \item Measure of WPLI in a time-frequency window.
    \end{enumerate}

\end{frame}

\begin{frame}{Brain Areas Connectivity (WPLI) -- Results (1/2)}
    \begin{figure}
        \centering
        \includegraphics[width=0.75\textwidth]{../media/plots/zone-zone_WPLI_allInRtbsi_allfreq_P2-LC1.png}
        \caption{WPLI between zone pairs frequency by frequency, in 0--250~ms after nose poke.}
    \end{figure}
    % Put figure on top of the plot
    \begin{tikzpicture}[remember picture,overlay]
        \node[xshift=-10cm,yshift=-5.5cm] at (current page.north east) {\includegraphics[width=0.45\textwidth]{../media/pictures/rat_brain_structures_networks.png}};
    \end{tikzpicture}
\end{frame}

\begin{frame}{Brain Areas Connectivity (WPLI) -- Results (2/2)}
    \begin{figure}
        \centering
        \includegraphics[width=0.75\textwidth]{../media/plots/zone-zone_WPLI_allRtbsi.png}
        \caption{WPLI between zone pairs across sessions in (6--10~Hz, 0--500~ms after nose poke).}
    \end{figure}
    % Put figure on top of the plot
    \begin{tikzpicture}[remember picture,overlay]
        \node[xshift=-10cm,yshift=-5.5cm] at (current page.north east) {\includegraphics[width=0.45\textwidth]{../media/pictures/rat_brain_structures_clean.png}};
    \end{tikzpicture}
\end{frame}

\section{Conclusion and Perspectives}


\begin{frame}{Conclusion and Perspectives}
    \begin{itemize}
        \item Phase reset of sniffing rhythm. No phase reset found in hippocampal LFP at the time reference chosen.
        \item Amplitude modulation of $\beta$ rhythm in OB follows the same pattern as rat performance.
        \item WPLI connectivity ($\theta$ band): two networks, OB or Hipp related, which seem to remain stable across sessions in the considered zone.
    \end{itemize}
    \hfill
    \includegraphics[width=0.5\textwidth]{../media/pictures/long-evans-3a.pdf}
\end{frame}


\begin{frame}{Tool box}
    \includegraphics[width=\textwidth]{../media/tools/toolbox.pdf}
\end{frame}

\begin{frame}
    \vfill
    \begin{center}
        \Huge
        Thank you for your attention.
        \normalsize
        \vfill
        \ccbysa%
        \vfill

        \includegraphics[height=1.5cm]{../media/logo/logo-crnl.png}
        \hfill
        \includegraphics[height=1.5cm]{../media/logo/logo-ueve.png}
    \end{center}
\end{frame}

% \begin{frame}[allowframebreaks,noframenumbering]{References}
%     \printbibliography[heading=none]
% \end{frame}

\end{document}

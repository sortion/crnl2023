# Internship - CRNL 2023

A monorepo for my 2023 internship at [*Lyon Neuroscience Research Center*](https://www.crnl.fr/).

This repository holds most of the code I wrote during the internship, except the contributions to [lfp_2014_cmo_nbuonviso_sniff_eeg_fos_laura](http://10.69.168.17/nicolas.fourcaud-trocme/lfp_2014_cmo_nbuonviso_sniff_eeg_fos_laura) (you must be on CRNL local network to access this).

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)


## Table of contents

- [notebooks](./notebooks/): Quarto project, as an attempt to follow the [literate programming](https://en.wikipedia.org/wiki/Literate_programming) paradigm, while keeping a written trace of what I did.
- [src](./src/): Some python scripts, mostly for data analysis.
- [workflow](./workflow/): Snakemake pipelines.
- [data](./data/): Data files (not included in the repository).
- [lfp](./lfp): analysis on Lefèvre's data as a git submodule.
- [report](./report/): internship report (LaTeX sources).
- [slides](./slides/): slides for the internship defense presentation (LaTeX sources).

## Environment setup

```bash
git clone http://10.69.168.17/samuel.ortion/internship-2023.git internship
cd internship
python3 -m venv .venv/crnl
source .venv/crnl/bin/activate
pip install -r requirements.txt
```

## Authors and acknowledgment

I thank my internship supervisor at CRNL, Dr. Nicolas Fourcaud-Trocmé.

## License

This project is licensed under the GNU GPLv3 License - see the [LICENSE](LICENSE) file for details.

## Project status

This internship is still in progress at CRNL.

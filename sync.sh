#!/usr/bin/env bash
: ' Sync this folder with cluster remote'

src=/home/sortion/Documents/cours/licence/L3/stage/crnl/project/
dest=node13:/home/samuel.ortion/Projects/intership/
rsync -avzu --exclude-from '.rsyncignore' $src $dest

if [[ $1 = "dual" ]]; then
    rsync -avzu --exclude-from '.rsyncignore' $dest $src
fi

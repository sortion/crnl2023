---
author: Samuel Ortion
date: 2023-05-15
updated: 2023-06-19
---

To keep a track of some of my internship progress, here is a logbook.

# Internship Timeline

## 2023-05-16 - *Starting date*

- Building visit, team introduction.
- Getting familiar with the project.


[...]

## 2023-05-25 - *Analysing Grion et al. (2016) data - Continuation*

Chose to limit windows extraction to only rats 3 (1301 to 204 expected windows, for LFP).

Uses a multithreaded python3 script to extract windows from the dataframe, and save them in an xarray dataset.


Launching with srun:
```bash
srun -n 1 --time=00:10:00 --cpus-per-task=2 --mem=16gb python3 ./src/grion_lfp_windows_multithreaded.py
```
It fails with `exceeding memory limit` error.

This is solved using the monothreaded version.


Updated the script to save the data as a pandas dataframe instead of an xarray DataSet, for easier manipulation, for the moment.

In order to be able to import python packages from .venv/..., user may want to load conda snakemake environment *before* the .venv.

All rats have their rayleigh test z-score plot around surface touch generated using this simple Snakemake workflow.

## 2023-05-26 - *Continuation of LFP analysis and Whisking*

Following an advice from the Bioinfo-fr.net discord, the Snakemake workflow rules is updated to use a dedicated env.

```snakemake
rule extract_windows:
    input: 'data/grion_db.out'
    output: 'data/grion_lfp_windows_r{R}_df.out'
    conda: 'envs/snake_env.yml'
    shell:
        """
        python3 src/grion_lfp_windows.py --rat {wildcards.R}
        """
```


This way the pipeline can be launched similarly with:
```bash
snakemake --cores 1
```
without having to worry about the execution environment.

Moreover, as SLURM is available on the CRNL cluster, one may move forward and use `--slurm` option to launch the pipeline on the cluster.

```bash
snakemake --cores 1 --slurm --jobs 2
```

### Extracting whisking windows as well

I duplicated the Snakemake pipeline, to do the same thing with whisking data. However, for the moment the execution fails.


### Pre-commit hooks considered harmful

I tried to use pre-commit hooks to automatically format my code, but it seems that it is not a good idea to do so, as it fails very often on commit, slowing down too much the workflow.

## 2023-05-30 - *Phase reset for Whisking data*

Achieve to extract windows around whiskers touch start (Grion et al.), followed by a Rayleigh test z-score plot.

Attempt to automate this analysis for L. Lefèvre LFP data.

## 2023-05-31 - *Phase reset continuation*


I amend the Snakemake workflow for extraction and analysis of LFP data (Grion et al.). A more versatile CLI was written, and the Snakefile was updated accordingly in `./workflow/analysis/flp_bysession.smk`.

To run this pipeline (allocating more than enough memory for the SLURM job):
```bash
snakemake --snakefile ./workflow/analysis/flp_bysession.smk --cores 1 --jobs 1 --slurm --default-resources mem_mb=32000
```

## 2023-06-01 - *Automate L. Lefèvre data analysis with Snakemake*

The pipeline needs to load `session.py` to retrieve rat list. However as this files depends on dataio, it needs the packages from the virtual environment, as Snakemake, installed from conda, executes from the conda environment.
So I needed to add some dependencies to the conda environment:
```bash
(snakemake) $ pip install -r requirements.txt
```
This is obviously not the cleanest way to do it.


Running the pipeline seems to require `--use-conda` option.
It is strange that the previous pipeline worked without this option, as I set `conda: ...` rule, to
```bash
snakemake --snakefile ./workflow/analysis/phase_reset.smk --cores 1 --jobs 2 --slurm --use-conda
```

### Reunion CMO (13h30)

### An attempt to improve reproducibility

Following an advice from bioinfo-fr.net discord server, I will try to use a unique conda environment containing snakemake and all python dependencies required to run the pipeline.


### Execute the pipeline

Command executed to run `lfp/` pipeline, extracting windows around nose poke.
```bash
snakemake --snakefile workflow/analysis/phase_reset.smk --jobs 10 --cores 2 --slurm --default-resources mem_mb=10000 --latency-wait 25
```

`--latency-wait` option seemed necessary as created files were detected after the default time threshold, leading to pipeline failure.


Sadly, the scripts has returned a lot of NaN-valued array for the Rayleigh test.

## 2023-06-02

### *Fixing pipeline and analyzing results*

### Attend the PhD defense of Amel Amara


## 2023-06-04

Fix report font, switch to English, and fit to requirements.

Creating a matplotlibrc ^[Customiser matplotlib (faire son matplotlibrc) <https://bioinfo-fr.net/customiser-matplotlib-faire-matplotlibrc>].

Generate the first plot for the report, using the `matplotlibrc` file.

## 2023-06-05

Using plotly for Zscore plotly.

Z-score of theta Hipp. LFP does not show a strong reproducible phase reset, while aligned on odour port entrance.

Trying to redo the same pipeline, using another time reference:
- Odor port exit (Time + Duration in `PP_df`)
- AssymBegin (Long respiration cycle).

Lauched both pipeline simultaneously.

Ran rayleigh tests.

### Fix error on AssymBegin aligned windows pipeline

```python
ValueError: setting an array element with a sequence. The requested array has an
inhomogeneous shape after 1 dimensions. The detected shape was (75,) +
inhomogeneous part.
```

### About tfmaps

Make measures in localized region in time frequency maps, try to study the evolution of such metrics across sessions, for rats.
Construct to do so a large dataframe from precomputed TF maps (xarray netCDF4 files).

## 2023-06-06

Started analysis of tf-maps.

### Attend a presentation on neuro pathology (ADEM, Optic Neuritis, etc.)

### Extracting measures

```bash
python3 -m tfmaps.analysis.tfmaps --output-file ./data/gamma-measures.csv --config-file ./tfmaps/configs/gamma_burst.yml
```

### Online conference "Memory Consolidation during Offline Waking Rest" Erin J. Wamsley


## 2023-06-07

Introducing normalization of measures in TF maps.

Generate massive theta TF maps plots.

Ran analysis on theta TF maps.

Refine TF windows.

Measure extraction script accept a yml file as configuration.


Extract the same measures on a trial by trial basis:
```bash
python3 -m tfmaps.analysis.tfmaps_bysession main --output-file ./data/gamma65_bysession.csv --config-file ./tfmaps/configs/gamma65.yml
```

## 2023-06-08

Analyzing `theta` TF maps more deeply.

Improve seaborn plots for measures.

Extract measures on a trial by trial basis.

### An attempt to render a 3D model of rodent brain

Using [`brainrender`](https://docs.brainrender.info/) python package, I attempted to render a 3D model of rodent brain (mouse), with the Allen Brain Atlas.

![3D rendering of mouse brain](../media/images/brainrender/screenshots/mouse_brainrender_crop.png)


### M2 Interns oral presentations (15h00)

## 2023-06-09

Generate new theta tfmaps on other time reference.

```bash
python3 -m tfmaps.analysis.precompute_tfmaps tf_theta_startFastRespi_params
```

Plot theta tfmaps:

```bash
python3 -m tfmaps.plots.plot_tfmaps theta_OnRespi
```

```bash
rsync -avzu /mnt/data/PAC_data_NFourcau/Generated\ figures/Avg_tfmaps/ /home/samuel.ortion/Projects/internship/notebooks/media/images/Avg_tfmaps/
```
Needed to use modified `timefreqtools` version.

theta maps aligned on PP showed a little "swirl" around PPend.

![A TF map in 2-20Hz band, aligned on PPend](../media/images/tfmaps/Avg_tfr_theta__Rtbsi22.png)

![A TF map in 2-20Hz band, aligned on PPend, rescaled on respiration signal](../media/images/tfmaps/Avg_tfr_theta_OnRespi__Rtbsi11.png)


Writing the first slides draft using `Beamer/metropolis` LaTeX theme.

## 2023-06-12

Narrow frequency window for gamma TF maps.

```bash
python3 -m tfmaps.analysis.measures --output-file ./data/measures_v2.csv --config-file ./tfmaps/configs/config_PPend.yml
```


## 2023-06-13

### Presentation on Free energy principle; *The free energy principle: foundations, and applications to neuroscience and artificial intelligence* by Lancelot Da Costa

### Pursue writing report

Regenerate plots with report style.


## 2023-06-14

Amend `measures.py` in order to extract measures, with different tfmaps source for measure and norm measure.


### A way to use slurm while quitting the session

```bash
nohup srun <command> & # @see https://unix.stackexchange.com/a/4006
```

```bash
nohup srun python3 -m tfmaps.analysis.measures --output-file ./data/measures_2.csv --config-file ./tfmaps/configs/config.yml &
```

Hopefully, this command will write the csv file without too much trouble this night.

## 2023-06-15

Unfortunately, the script ran out of memory.

Trying again:

```bash
 nohup srun --mem=32G python3 -m tfmaps.analysis.measures --output-file
 ./data/measures_2.csv --config-file ./tfmaps/configs/config.yml &
```


Renamed quarto notebooks, in order to see them in the chronological order in the file explorer.

```bash
#!/usr/bin/env bash
: '
Add a number prefix to a set of file in a given directory
such as the number reflects the chronology of file creation
in order for the files to appear by date on file explorer
'
set -eou pipefail

files_by_creation_order() {
        local ext
        ext="$1"
        ls -rc *.$ext
}

ext="$1"
number=0
for file in $(files_by_creation_order "$ext"); do
        number=$(expr $number + 1)
        mv "$file" "${number}_${file}"
done
```
```bash
chmod +x ~/bin/add_number_prefix.sh
cd notebooks/notebooks/sniffing
add_number_prefix.sh ipynb
```

I changed the file reference in `_quarto.yml` accordingly.


## 2023-06-19 - *Connectivity analysis*

In `/mnt/data/PAC_data_NFourcau/precomputePAC/connectivity_wpli` there are the following precomputed xarray connectivity matrices.

```bash
ls /mnt/data/PAC_data_NFourcau/precomputePAC/connectivity_wpli
```

```text
gammaComplex  gammaNotchComplex  thetaComplex  thetaComplexInLicks
```


## 2023-06-20 -

### *Tu fais quoi au CRNL*

- Jean Baptiste Van Der Henst : *L’enfance du pouvoir : une question de genre ?*

- Claude Gronfier - *Mon voyage en chronobiologie*

> Il faut regarder les courbes
        Gabrielle Brandenberger.

> Hope for the best, plan for the worst.
        Chuck Czeisler

[...]

## 2023-06-26

Submit report for notation.

Try to find out $\theta$-hippocampal LFP phase reset down modulation through learning: not conclusive.


## 2023-06-27

Rerun the phase reset workflow for all rat sniff phase reset, to find a down modulation: not very conclusive either.

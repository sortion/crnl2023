    Bonjour à tous,

    Les soutenances de stages auront lieu dans un mois, le lundi 03/07, à partir de 9H00, en salle 232 IBGBI. L'ordre de passage sera l’ordre alphabétique (l’heure exacte de votre convocation vous sera indiquée plus tard).

    La durée de présentation sera de 15 min. Elle sera suivie de 10 min de questions.
    Le fichier de votre présentation devra être au format PDF : vous me l'enverrez avant, ou bien l'apporterez sur une clef USB (formatage compatible Linux) le jour même.

    Avant cela, vous devrez m'envoyer vos rapports par email, au plus tard le mercredi 28/06, 23h59.

    La rédaction pourra être en anglais ou en français.

    Consignes pour le format :
    - police avec empattement, taille 12,
    - interligne 1,5,
    - légende et numéro pour chaque figure et tableau,
    - numéros de pages.

    Le rapport comportera les rubriques suivantes :
    - Abstract
    - Introduction
    - Methods
    - Results and Discussion
    - Conclusions and Perspectives
    - References

    Il n'y a pas de consigne sur le nombre de pages. Néanmoins, l'utilité des informations que vous choisirez d'inclure ou d'omettre sera jugée.
    Comme il s'agit d'un stage court niveau L3, c'est votre compréhension du sujet et de ce que vous avez fait qui sera évaluée, plus que la quantité de résultats obtenus.

    N’hésitez pas à me contacter pour d’éventuelles questions.
    Cordialement,

    Dr Guillaume Postic

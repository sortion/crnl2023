%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Internship Report- L3 Informatique - Sciences de la vie - Semester 6
% Centre de Recherche en Neurosciences de Lyon
%
% May-July 2023
%
% Version: v0.0.3
% Author: Samuel Ortion
% Date: 2023-07-03
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[
    twoside
]{sty/crnl}

\title{Rat electrophysiological signal analysis}
\subject{Internship report}
\author{\orcidlink{0009-0001-0971-497X} Samuel Ortion}
\studies{L3 Licence double Informatique - Sciences de la vie \\ University of Évry -- Paris-Saclay University}
\supervisor{\orcidlink{0000-0002-2211-7547} Nicolas Fourcaud-Trocmé}
\affiliation{Lyon Neuroscience Research Center}
\date{From May 15th to July 7th 2023}

\makeatletter
\hypersetup{
    pdfauthor={\@author},
    pdftitle={\@title - L3 internship report - \@author},
    pdfsubject={\@subject},
    pdfkeywords={L3, internship, python, neuroscience, rat, olfaction, electrophysiology, LFP, respiration}
}
\makeatother

\addbibresource{../references.bib}

\makeglossaries
\input{acronyms.tex}

\begin{document}

\input{layout/firstpage.tex}

\pagenumbering{roman}

\newpage

\begin{abstract}
    In previous experiments, rats were trained to perform an odor discrimination task.

    Phase reset is known to be a common phenomenon in brain rhythms after a sensory stimulus. We investigated phase reset of brain rhythms, as well as respiratory signal. We found a phase reset in sniffing rhythms. The other signals did not show a conclusive phase reset, on the time references we used.

    We also searched for amplitude modulation of brain rhythms through learning, using time-frequency maps. We reproduced the result of $\beta$ frequency up modulation during learning and found some other interesting modulations in  different time-frequency windows.

    Finally, we investigated connectivity between recorded brain regions. We studied more specifically the connectivity of hippocampal $\theta$ rhythm and respiratory / olfactory bulb lower $\theta$ rhythm, to study the evolution of brain networks during odor learning.
\end{abstract}

\vfill

\doclicenseThis % Print license

\newpage

{
    \hypersetup{hidelinks}
    \tableofcontents
    \listoffigures
    \listoftables
}

\newpage

% Abbreviations
\printglossaries

\newpage

\pagestyle{fancy}

\pagenumbering{arabic}

\section{Introduction}

Olfaction is undoubtedly an important sense for rodents. Indeed, it has a strong influence on their behaviour, as it is involved in finding food as well as social relationships.
Odor sampling depends on respiratory rhythms: odorant molecules are driven onto the olfactive epithelium at each inspiration / expiration cycle.
It has been shown that respiration modulates olfactory neurons activity \cite{buonvisoRespiratoryModulationOlfactory2006}, as well as non-olfactory neurons \cite{juventinRespiratoryRhythmEntrains}.

\paragraph{LFP and brain rhythms}

In the present work, we used intra-cranial recordings of local field potentials (LFP).
LFP are the result of the sum of the activity of neurons in the vicinity of a recording electrode, implanted inside the brain tissue. LFP recordings show often rhythmic activity or oscillations (see \cref{fig:signal_extract})

Oscillations of LFP are an important feature of the brain. Oscillations appear to be a fundamental mechanism for the coordination of neuronal activity, as well as communication between brain regions, for instance \cite{buzsakiRhythmsBrain2006}.

Olfactory system is characterized by numerous LFP oscillations. These oscillations are either slow, related to the respiratory rhythm (especially in the \Gls{OB}), or faster, such as $\beta$ and $\gamma$ rhythms which are rather related to perception and memory. % Citation ?

The aim of this internship was to study the evolution of such brain rhythms during the learning of an olfactory discrimination task in rats, by analyzing the phase, amplitude and synchronization of these oscillations between distant brain areas.

\section{Methods and Results}

The analyses were performed on data acquired by \citet{lefevreSignificanceSniffingPattern2016} in 2014 at CRNL.

\subsection{Experiments}

The experiments are described in \cite{lefevreSignificanceSniffingPattern2016,fourcaud-trocmeHighBetaRhythm2019}. Briefly: thirteen rats were implanted with stainless steel LFP recording electrodes soldered to copper wire in \glsxtrfull{OB}, \glsxtrfull{AP}, \glsxtrfull{PP}, \glsxtrfull{OT}, dorsal \glsxtrfull{Stri}, dorsal CA1 of the \glsxtrfull{Hipp}, and \glsxtrfull{Cereb}.
For reference, a diagram of the location of the recorded brain structures is shown in \cref{fig:rat_brain_structures}.

LFP signals were transmitted remotely to the acquisition machine.
Rats were placed in a whole body plethysmograph allowing to record respiratory rhythms (as described in \cite{hegoburuRUBCageRespirationUltrasonic2011}).

\begin{figure}[hbp!]
    \centering
    \includegraphics[width=0.75\textwidth]{../media/pictures/rat_brain_structures.pdf}
    \caption[Rat brain structures where LFP probes were implanted]{Localization of rat brain structures where LFP probes were implanted. \glsxtrfull{LFP} were recorded in \glsxtrfull{OB}, \glsxtrfull{AP},
        \glsxtrfull{PP}, \glsxtrfull{OT}, \glsxtrfull{Stri}, dorsal \glsxtrfull{Hipp} (CA1 cells) and \glsxtrfull{Cereb}.}
    \label{fig:rat_brain_structures}
\end{figure}

\begin{figure}
    \centering
    \begin{subfigure}{0.45\columnwidth}
        \centering
        \includegraphics[height=10em]{../media/pictures/experiment_scheme.png}
        \caption{Odorant pair discrimination}
    \end{subfigure}
    \begin{subfigure}{0.45\columnwidth}
        \centering
        \includegraphics[height=10em]{../media/pictures/whole-body-plethysmograph.png}
        \caption{Whole body plethysmograph}
    \end{subfigure}
    \caption[Experimental setup]
    {
        Experimental setup of the discrimination training. \textbf{(a)} Rats were trained to discriminate a pair of odorants. A trial was initiated by rat nose poke on odor port (pink circle). The rat had 6~s to make a choice. If the rat chose the correct port, it was rewarded with water. \textbf{(b)} The experiment took place in a whole body plethysmograph allowing to record respiratory rhythms.
        (source: \cite{lefevreSignificanceSniffingPattern2016})
    }
    \label{experimental-setup}
\end{figure}

A central odor port with capacitive sensor allowing nose poke detection delivered one of a pair of odor.
Two lateral reward ports were able to deliver water. The right or left reward port were associated with the odor. The rat had only 6~s to chose the correct port. Whenever the rat chose the incorrect port, no water reward was delivered (see \cref{experimental-setup}a).

Several experiment phases with distinct odor pairs were performed. Phases P1, P2 and P3 were associated with distinct odor pairs. In these phases, several test days occurred, namely S1, S2, \ldots, S\textsubscript{$n$}, LC0 LC1, \ldots, LC\textsubscript{$n$}, where LC1 is the first test day where rat obtained more than 80\% of success.

The experiment took place in a whole body plethysmograph allowing to record respiratory rhythms (see \cref{experimental-setup}b).

\begin{figure}[hbp!]
    \centering
    \includegraphics[width=0.75\textwidth]{../media/pictures/signal_zoom_extract_Rtbsi28_P1_LC2_annotated.png}
    \caption[Signal extract available in Lefèvre's dataset with rhythms annotations]
    {\gls{LFP} and respiration signals extract, available from Lefèvre's dataset
        \cite{lefevreSignificanceSniffingPattern2016,fourcaud-trocmeHighBetaRhythm2019}.
        Rats \glsxtrfull{LFP} were recorded in \glsxtrfull{OB}, \glsxtrfull{AP},
        \glsxtrfull{PP}, \glsxtrfull{OT}, \glsxtrfull{Stri}, and dorsal \glsxtrfull{Hipp} (CA1).
        \glsxtrfull{Respi} signal was recorded with a plethysmograph. Stereotypical rhythms (theta, beta, Respi, and gamma) are annotated on the figure.
    }
    \label{fig:signal_extract}
\end{figure}

\subsection{Analysis}

All subsequent data analyses have been performed using home-made Python scripts with the help of Python libraries such as numpy, pandas, scipy, xarray, and others.
Basic SnakeMake analysis pipelines were written to automate the analyses, and launch them easily on the Slurm cluster \cite{molderSustainableDataAnalysis2021}.

\subsubsection{Phase reset}

In order to detect if the animals synchronized their respiration and slow brain rhythms at the starting of the trial, a phase reset analysis was conducted.

Signals were band pass filtered in 5-15~Hz (slow rhythms were privileged, as higher frequencies are unlikely to show up phase resets). Then, the phases of the signals were computed using Hilbert transform.
Trial phases were aligned on the time of nose poke in a time window of 2~s on each side. An example of LFP signal phase around nose poke is shown in \cref{fig:lfp_signal_filtering}.
The phase of each trial at a given time were stacked and a Rayleigh test was performed to detect phase synchronization.
Then, the z-scores of the Rayleigh test $p$-values were computed.

A phase reset plot of Rayleigh test $p$-value Z-scores for respiratory signal is presented \cref{fig:rayleigh_zscore_sniff_Rtbsi11_1-15Hz}.
A phase reset of respiratory signals occurs for rats entering the odor port.

No phase reset in $\theta$-hippocampal LFP signal could be identified.
This may be due to the small random latency between detected nose poke and real release of the odor, which could lead to an asynchrony between brain rhythms associated with the treatment of the odorant signal and decision making. It may also be caused by inherent variability in latency for $\theta$ rhythm induction in the rat brain.


% TODO change the figure, to data from Lefèvre et al.
\begin{figure}[hbtp!]
    \centering
    \includegraphics[width=0.75\textwidth]{../media/plots/lfp_signal_filtering_lefevre.pdf}
    \caption[Phase of \gls{LFP} signal after filtering]{Phase of \gls{LFP} signal after filtering in 5-15~Hz band, from hippocampal LFP of rat \texttt{Rtbsi11}. Phase of signal is computed using Hilbert transform. A time window of 2 seconds on each side of the nose poke is shown.}
    \label{fig:lfp_signal_filtering}
    \includegraphics[width=0.75\textwidth]{../media/plots/rayleigh_zscore_sniff_Rtbsi11_1-15Hz.pdf}
    \caption[Respiratory signal phase reset for rat \texttt{Rtbsi11}]{
        Respiratory signal phase reset for rat \texttt{Rtbsi11}.
        Signal were filtered in 5-15~Hz band. Hilbert transform was used to compute the phase of the signal.
        Phases of signal for each trial were aligned to entry in odor port in 2~s windows on each side around this timestamp.
        Rayleigh test was performed on phase distributions at each time step. Then, the Z-scores of these $p$-values were computed.
    }
    \label{fig:rayleigh_zscore_sniff_Rtbsi11_1-15Hz}
\end{figure}

\subsubsection{Time-frequency maps and amplitude modulation}

It has already been shown that $\beta$ oscillations in the \gls{AP} were up modulated throughout the learning of this very task \cite{fourcaud-trocmeHighBetaRhythm2019}. We wanted to look at the modulation of other brain rhythms.

In order to study the modulation of brain rhythms amplitude during learning, we measured the intensity of the signal in specific time-frequency windows.

Time-frequency power maps were computed using standard Morlet wavelet transform, with $\sigma = 5\pi$ \cite{fourcaud-trocmeHighBetaRhythm2019}. Time Frequency maps were computed for each trial's whole time range.

TF-maps averaged across trials were computed and showed some interesting frequency modulation pattern, when trial variability was smoothed. One of such an averaged \Gls{TF-map} is shown in \cref{fig:tfmaps}. In this figure, a region of interest is outlined in $\gamma$ frequency band. Such regions, noticed in averaged TF-map were used to compute local amplitude modulation across sessions and within a session in different time-frequency windows. The median in a reference zone in the same frequency band but before the odor port entry ($-1$-$-0.6$~s), was used to normalize the amplitude modulation.
Measures were computed on a trial by trial basis. The modulation in amplitude of the signal in $\beta$ band (12 - 30~Hz) was measured as the 3\textsuperscript{rd} quartile between $-0.4$ and $0.2$~s around odor port exit.

\begin{figure}[hbp!]
    \centering
    \begin{subfigure}{0.45\columnwidth}
        \centering
        \includegraphics[height=10em]{../media/plots/gamma_amplitude_measure_zone_Rtbsi16.pdf}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.45\columnwidth}
        \centering
        \includegraphics[height=10em]{../media/plots/theta_tfmap_Rtbsi35_Hipp_sq.pdf}
        \caption{}
    \end{subfigure}
    \caption[Examples of TF-maps averaged across trials]{Examples of TF-map, averaged across trials and aligned on odor release end.
        \textbf{(a)}
        In $\gamma$ frequency band for rat \texttt{Rtbsi16} in \Gls{OB}. A region of interest is outlined in $\gamma$ frequency band.
        \textbf{(b)}
        In $\theta$ frequency band for rat \texttt{Rtbsi35} in \Gls{Hipp}, a \gls{TF-map} showing a little \textquote{swirl} in frequency modulation.
        An harmonic of the modulated spot is also visible, at twice the frequency.
        A red and an orange square represent the time-frequency windows used to measure amplitude modulation of upper and lower $\theta$ respectively.
        Greenish color indicates higher power in the corresponding time-frequency bin.
    }
    \label{fig:tfmaps}
\end{figure}

A modulation in amplitude of the LFP signal in $\beta$ band around odor port exit appears correlated to rat performance, in different brain regions, such as olfactory bulb (see \cref{fig:beta-amplitude-modulation}).
This modulation can be seen in different other recorded brain regions, such as \Gls{OT}, \Gls{Stri} and \Gls{PP}.
As shown by \citet{fourcaud-trocmeHighBetaRhythm2019}, this modulation is also present in \Gls{AP}.
The increase in $\beta$-amplitude appears in the 25 first sessions of a given phase, then tend to stabilize (\cref{fig:beta-increase-in-session}).

\begin{figure}[hbp!]
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{../media/plots/beta_amplitude_modulation_ob.pdf}
        \caption{$\beta$ modulation in \glsxtrfull{OB}}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{../media/plots/rat_performance.pdf}
        \caption{Rat performance}
    \end{subfigure}
    \caption[Amplitude modulation of $\beta$ rhythm in olfactory bulb follows the same pattern as rat performance across sessions.]{
        Amplitude modulation of $\beta$ rhythm in olfactory bulb follows the same pattern as rat performance across sessions.
        \textbf{(a)} Amplitude modulation of $\beta$ rhythm in olfactory bulb across sessions, measured with 3\textsuperscript{rd} quartile of $\beta$ amplitude in -400~ms up to 100~ms around odor port exit, in 12-30~Hz band.
        New odor pair is introduced at each change in phase (P1-P2-P3-P1), pair of odor periods are represented as color bands.
        \textbf{(b)} Rat performance across sessions, as number of correct trials over total number of trials. A trial is considered as correct whenever the rat headed first to the odor port that were associated with the odor presented, thus receiving the water reward.
        \textbf{(a) \& (b)} Error bars corresponds to 95\% confidence interval for the mean.
    }
    \label{fig:beta-amplitude-modulation}

    \centering
    \includegraphics[width=0.5\textwidth]{../media/plots/beta_ob_growth_t25.pdf}
    \caption[$\beta$ increases inside sessions in olfactory bulb]{$\beta$ increase in sessions in \gls{OB}.
        Amplitude modulation of $\beta$ rhythm in olfactory bulb inside sessions, measured on 3\textsuperscript{rd} quartile of $\beta$ amplitude in -400~ms up to 100~ms around odor port exit, in 12-30~Hz band. All sessions are mixed and the first 25 trials in a session are selected.
        The line is the average across rats of the average measure across sessions for each rat. Error bars represents 95\% confidence interval for the mean.}
    \label{fig:beta-increase-in-session}
\end{figure}

Among other brain rhythms studied, the $\theta$ rhythm showed up an interesting modulation pattern, as presented in \cref{fig:tfmaps}B. In a manner similar to $\beta$ amplitude measure, the 3\textsuperscript{rd} quartile of signal intensity in $-2$ to $-1$~s around odor port exit in $7.5$-$10$~Hz band (upper theta), and in $-0.1$ to $0.1$~s in $6$-$7$~Hz band (lower theta).
The upper $\theta$ rhythm is up-modulated through learning, while the lower $\theta$ rhythm is down-modulated (\cref{fig:theta-opposite-modulations}).

\begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{media/plots/theta_different_modulations.pdf}
    \caption[Theta opposite modulations across sessions.]{
        $\theta$ opposite modulations across sessions in \glsxtrfull{OT}.
        Upper $\theta$ measured in $-2$ to $-1$~s around odor port exit in $7.5$-$10$~Hz band (left panel), lower $\theta$ measured in $-0.1$ to $0.1$~s in $6$-$7$~Hz band (right panel).
        New odor pair is introduced at each change in phase (P1-P2-P3-P1), pair of odor periods are represented as color bands.
        Error bars corresponds to 95\% confidence interval for the mean across rats.
    }
    \label{fig:theta-opposite-modulations}
\end{figure}


\subsubsection{Connectivity measures}

A measure of phase synchronization between distant brain areas was used to decipher the evolution of brain connectivity through learning.

\paragraph{WPLI}

\glsxtrfull{wPLI} is an indicator on signal connectivity, based on the phase difference between two signals.

This metrics was chosen as it tries to reduce the impact of volume conduction, which usually causes the two signals to share the exact same phase, even if there is no real neuronal connectivity between the two regions \cite{vinckImprovedIndexPhasesynchronization2011,bastosTutorialReviewFunctional2016}.
\Gls{wPLI} ranges from 0 to 1, where 0 indicates no detected phase synchronization (or exact same phase, in case of volume conduction), and 1 indicates a constant phase difference between the two signals.

\Gls{wPLI} was computed using the \texttt{spectral\_connectivity} Python package. It was estimated frequency by frequency on precomputed wavelet spectra.

A time-frequency map of \gls{wPLI} was computed for each session and for each pair of probes. One of such a map is presented in \cref{fig:wpli-tfmap}. On this map, there is a stronger connectivity between \gls{OB} and \gls{PP} probes in 12-15~Hz, close to the nose poke event.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{../media/plots/WPLI_tfmap_BO-PP_Rtbsi25_22.png}
    \caption[wPLI between olfactory bulb and posterior piriform cortex]{
        wPLI between \Glsxtrfull{OB} and \Glsxtrfull{PP} probes as time-frequency map for rat \texttt{Rtbsi25} in session P1-LC2. wPLI were computed for each frequency from standard Morlet wavelet spectra.
        The median wPLI across frequency is presented next to the corresponding TF-map.
        Blue color indicates low \gls{wPLI} values, whereas red and green colors indicates higher \gls{wPLI} values (i.e higher connectivity).
    }
    \label{fig:wpli-tfmap}
\end{figure}

On such a connectivity measures, available on whole time-frequency domain, a measure in a narrower zone (6-10~Hz, 0-500~ms) was computed using a method similar to the one used for TF-map amplitude modulation measures (\cref{fig:wpli-across-sessions} presents this modulation across sessions).
The connectivity between region pairs seems to remain relatively stable across sessions, despite rat learning, at least in the time-frequency window considered.

On the other hand, a measure of \gls{wPLI} was computed frequency by frequency, in order to observe the modulation of connectivity in different frequency bands. \Gls{wPLI} measure through frequency is presented for each pair of brain regions in session P1-S1 in \cref{fig:wpli-in-session-against-freq}.
Between \gls{OB} region and other regions such as \gls{AP}, \gls{PP}, \gls{Stri} and \gls{OT}, there is a strong \gls{wPLI} peak in 6-9~Hz, in respiratory and $\theta$ frequency bands.
We can see another strong \gls{wPLI} peak (and a second peak which may correspond to a harmonic) between \gls{Hipp} and \gls{AP}, \gls{PP}, \gls{Stri} or \gls{OT}.
These observations lead to a model of neuronal network connectivity: respiratory rhythm, that originates from \gls{OB} propagates to \gls{AP}, \gls{PP}, \gls{Stri} and \gls{OT}. Hippocampal $\theta$ rhythm, propagates to \gls{AP}, \gls{PP}, \gls{Stri} and \gls{OT} (\cref{fig:wpli-in-session-against-freq}).

\begin{figure}[p]
    \centering
    \includegraphics[width=0.8\textwidth]{../media/plots/zone-zone_WPLI_allRtbsi.png}
    \caption[wPLI across sessions for all rats in a time-frequency region]{
        \gls{wPLI} across sessions for all rats in a time-frequency region.
        \gls{wPLI} is computed between each zone pairs, given by line and column.
        A measure of mean \gls{wPLI} is computed in 6-10~Hz, 0-500~ms time-frequency region.
        The sessions presented are the same as in \cref{fig:beta-amplitude-modulation}.
        Each lighter blue lines represents a rat. The bolder line is the average of all rats.
    }
    \label{fig:wpli-across-sessions}
    \includegraphics[width=0.8\textwidth]{../media/plots/zone-zone_WPLI_allInRtbsi_allfreq_P2-S1.png}
    \caption[WPLI connectivity modulation in frequency, for rats in session P2-S1]{\Gls{wPLI} connectivity modulation in frequency, for rats in session P2-S1.
        \Gls{wPLI} is computed between each zone pairs, given by line and column, in whole time frequency domain from standard Morlet wavelet spectra. On wPLI matrix, a 25~ms time range after nose poke was selected and the median of wPLI was computed for each frequency, by 0.5~Hz step.
        Each lighter green lines represents a rat. The bolder line is the average of all rats.}
    \label{fig:wpli-in-session-against-freq}
\end{figure}

\clearpage % Avoid figure to mess with references

\section{Conclusions and Perspectives}

The analyses of the previously collected data allowed to observe a phase reset of the respiratory rhythms at the entry in odor port. We could not show the same phase reset for LFP signal in hippocampal $\theta$ band, probably due to the variable latency between nose poke and odor release, or the variability in $\theta$ rhythm induction after sensory stimuli.
We found a modulation in $\beta$ LFP oscillations in several brain regions, such as \gls{OB} following a similar pattern as rat performance across session and within session throughout rats' task learning.
Finally, we could not observe a strong modulation in \glsxtrfull{wPLI} connectivity between brain regions.

To go further, one could try to compute phase reset of LFP signal in hippocampal $\theta$ band using other reference timestamps, such as the time of the slower respiratory cycle occurring at the end of odor sampling, or at the exit of the odor port, for instances.
A more precise analysis of connected neural networks could be explored, using graph theory backed technics \cite{barabasiNeuroscienceNeedsNetwork2023}, for instance.

During this internship, I learned the basics of neuroscience, and some of the techniques used to analyze \gls{LFP} signals.
This internship was the occasion to discover a bit of signal processing methods.
I learned how to use Slurm on the lab computing cluster, as well as the basics of pipeline programming with SnakeMake too.

Throughout this two months, I had the opportunity to attend several presentations from CRNL researchers, allowing me to perceive a bit of the diversity of topics studied there.


\printbibliography

\end{document}

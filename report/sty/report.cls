%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CNRL Intership Report Class
%
%
% Version: v0.0.1
% Author: Samuel ORTION
% Date: 2023-04-26
% License: LPPL 1.3c
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------
% Class options
%----------------------------------------------------------------------------
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{sty/report}[2023-04-26 v0.0.1 CNRL Intership Report Class]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ProcessOptions\relax

\LoadClass{report}

%----------------------------------------------------------------------------
% Metadata options
%----------------------------------------------------------------------------

\global\let\@student\@undefined
\global\let\@supervisor\@undefined
\global\let\@affiliation\@undefined

\newcommand{\student}[1]{\gdef\@student{#1}}
\newcommand{\supervisor}[1]{\gdef\@supervisor{#1}}
\newcommand{\affiliation}[1]{\gdef\@affiliation{#1}}

%----------------------------------------------------------------------------
% Required packages
%----------------------------------------------------------------------------

\RequirePackage{luatextra}

% Font
\RequirePackage{fontspec}
\setmainfont{TeX Gyre Pagella}
\RequirePackage{ulem}
\RequirePackage{microtype}
\RequirePackage{csquotes}
\RequirePackage{soul}

% Language
\RequirePackage{polyglossia}
% \setmainlanguage{french}
% \setotherlanguage{english}

% Floats and graphics
\RequirePackage{graphicx}
\RequirePackage{float}
\RequirePackage{caption}
\RequirePackage{subcaption}
\RequirePackage{xcolor}
\RequirePackage[usenames,svgnames,table]{xcolor}

% Define colors
\definecolor{fgRed}{RGB}{234,83,70}
\definecolor{crnlBlue}{HTML}{#1149b1}
\colorlet{fgBlue}{crnlBlue}

% Bibliography
\RequirePackage[
    maxcitenames=2,
    style=nature,
    backend=biber
]{biblatex}
\RequirePackage{doi}

% Margins
\RequirePackage[margin=2.5cm]{geometry}

% SI units
\RequirePackage{siunitx}
\RequirePackage{icomma}
\RequirePackage{textcomp}

% Check marks
% \RequirePackage{pifont}
% \newcommand{\cmark}{\ding{51}}
% \newcommand{\xmark}{\ding{55}}

\RequirePackage[
	bookmarks=true, bookmarksnumbered=true, bookmarksopen=true,
	unicode=true, colorlinks=true, linktoc=all, %linktoc=page
	linkcolor=fgRed, citecolor=fgRed, filecolor=fgRed, urlcolor=fgRed,
	pdfstartview=FitH,
    pdfencoding=auto    % avoid encoding problems in PDF bookmarks (French)
]{hyperref}
\RequirePackage{url}

\RequirePackage[nameinlink]{cleveref}

% Data tables
\RequirePackage{csvsimple}


% Math and symbols
\RequirePackage{amsmath}
\RequirePackage{amsthm}
\RequirePackage{amssymb}
\RequirePackage{shadethm}

\newtheorem{theorem}{Théorème}
\newshadetheorem{lemma}[theorem]{Lemme}
\newshadetheorem{proposition}[theorem]{Proposition}
\newshadetheorem{corollary}[theorem]{Corollaire}
\newshadetheorem{fact}[theorem]{Fait}
\newshadetheorem{Hypothesis}[theorem]{Hypothèse}
\newshadetheorem{remark}[theorem]{Remarque}
\newshadetheorem{definition}[theorem]{Définition}

% Custom caption style
\captionsetup{
    labelfont={color=fgBlue},
}

\RequirePackage{tikz}

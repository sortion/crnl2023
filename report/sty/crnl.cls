%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CNRL Intership Report Class
%
%
% Version: v0.0.1
% Author: Samuel ORTION
% Date: 2023-04-26
% License: LPPL 1.3c
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------
% Class options
%----------------------------------------------------------------------------
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{sty/crnl}[2023-04-26 v0.0.1 CNRL Intership Report Class]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

\LoadClass[
    12pt,
    a4paper
]{article}

%----------------------------------------------------------------------------
% Metadata options
%----------------------------------------------------------------------------

\global\let\@supervisor\@undefined
\global\let\@affiliation\@undefined
\global\let\@major\@undefined
\global\let\@subject\@undefined
\global\let\@studies\@undefined

\newcommand{\supervisor}[1]{\gdef\@supervisor{#1}}
\newcommand{\affiliation}[1]{\gdef\@affiliation{#1}}
\newcommand{\major}[1]{\gdef\@major{#1}}
\newcommand{\subject}[1]{\gdef\@subject{#1}}
\newcommand{\studies}[1]{\gdef\@studies{#1}}

%----------------------------------------------------------------------------
% Required packages
%----------------------------------------------------------------------------

\RequirePackage{luatextra}

% Font
\RequirePackage{fontspec}
\setmainfont{Latin Modern Roman}
\usepackage[normalem]{ulem} % normalem to prevent underlining of emphasis (especially in bibliography)
% \RequirePackage{microtype}
\RequirePackage{soul}
\RequirePackage{csquotes}

\RequirePackage{setspace}
\onehalfspacing% 1.5 line spacing

\renewcommand{\familydefault}{\rmdefault}

% Use Roman for headings
\RequirePackage{sectsty}
\allsectionsfont{\rmfamily}

% Idem for TOC
\RequirePackage{tocloft}
\renewcommand{\cftsecfont}{\rmfamily}
\renewcommand{\cftsecpagefont}{\rmfamily}
\renewcommand{\cfttoctitlefont}{\Large\bfseries\rmfamily}
% Idem for list of figures
\renewcommand{\cftloftitlefont}{\Large\bfseries\rmfamily}
% Idem for list of tables
\renewcommand{\cftlottitlefont}{\Large\bfseries\rmfamily}

% Language
\RequirePackage{polyglossia}
\setmainlanguage{english}
\setotherlanguage{french}

% Floats and graphics
\RequirePackage{graphicx}
\RequirePackage{float}
\RequirePackage{caption}
\RequirePackage{subcaption}
\RequirePackage[usenames,svgnames,table]{xcolor}

% Define colors
\definecolor{fgRed}{RGB}{234,83,70}
\definecolor{crnlBlue}{HTML}{1149b1}
\colorlet{fgBlue}{crnlBlue}

% Bibliography
\RequirePackage[
    maxcitenames=2,
    style=numeric,
    backend=biber,
    citestyle=numeric-comp,
    natbib=true
]{biblatex}
\RequirePackage{doi}
\RequirePackage{xurl}


% Margins
\RequirePackage[margin=2.5cm]{geometry}

% SI units
\RequirePackage{siunitx}

% Check marks
\RequirePackage{pifont}
\newcommand{\cmark}{\ding{51}}
\newcommand{\xmark}{\ding{55}}

% Data tables
% \RequirePackage{csvsimple}


% Math and symbols
\RequirePackage{amsmath}
\RequirePackage{amsthm}
\RequirePackage{amssymb}
\RequirePackage{shadethm}

\newtheorem{theorem}{Théorème}
\newshadetheorem{lemma}[theorem]{Lemme}
\newshadetheorem{proposition}[theorem]{Proposition}
\newshadetheorem{corollary}[theorem]{Corollaire}
\newshadetheorem{fact}[theorem]{Fait}
\newshadetheorem{Hypothesis}[theorem]{Hypothèse}
\newshadetheorem{remark}[theorem]{Remarque}
\newshadetheorem{definition}[theorem]{Définition}

% Custom caption style
\captionsetup{
    labelfont={color=fgBlue, bf},
    figurename=Figure,
    tablename=Table
}

\RequirePackage{tikz}

% \RequirePackage{sty/fancy}

% \RequirePackage{minted}
% \usemintedstyle{vs}

\RequirePackage{hyperref}
\hypersetup{
    bookmarksnumbered=true,
    bookmarksopen=true,
    unicode=true,
    colorlinks=true,
    linktoc=all, %linktoc=page
    linkcolor=fgRed,
    citecolor=fgRed,
    filecolor=fgRed,
    urlcolor=fgRed,
    pdfstartview=FitH,
    pdfencoding=auto    % avoid encoding problems in PDF bookmarks (French)
}

\RequirePackage[
    nameinlink,
    noabbrev
]{cleveref}

\usepackage[
    abbreviations,         % create "abbreviations" glossary
    nomain,                % don't create "main" glossary
    stylemods=longbooktabs, % do the adjustments for the longbooktabs styles,
    automake
]{glossaries-extra}

% Reset glossary words color to black
\renewcommand*{\glstextformat}[1]{\textcolor{black}{#1}}


\RequirePackage{orcidlink}

\RequirePackage[
    type={CC},
    modifier={by-sa},
    version={4.0},
]{doclicense}

% Add fancy heading
\usepackage{fourier-orns}
\usepackage{fancyhdr}
\renewcommand{\headrule}{%
    \vspace{-8pt}\hrulefill
    \raisebox{-2.1pt}{\quad\decofourleft\decotwo\decofourright\quad}\hrulefill}

\setlength{\headheight}{17pt}

\pagestyle{fancy}

% Write only section name in header
\fancyhf{}

\fancyhead[LE]{\nouppercase{\rightmark\hfill\leftmark}}
\fancyhead[RO]{\nouppercase{\leftmark\hfill\rightmark}}
\fancyfoot[LE,RO]{\hfill\thepage\hfill}

\RequirePackage{subcaption}

"""
PyQt GUI to explore Grion et al. data.
"""

import sys

import PyQt5.QtWidgets as QT

from .viewer import get_viewer

WINDOW_SIZE = 500


class ExploreWindow(QT.QMainWindow):
    """
    PyQt6 GUI to explore Grion et al. data.
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Explore Grion et al. data")
        self.resize(WINDOW_SIZE, WINDOW_SIZE)

        self.mainWidget = QT.QWidget()
        self.setup()

    def setup(self):
        get_viewer()
        # self.setCentralWidget(self.viewer)


def main():
    pygrionApp = QT.QApplication([])
    pygrionWindow = ExploreWindow()
    pygrionWindow.show()
    sys.exit(pygrionApp.exec())


if __name__ == "__main__":
    main()

import os

import pandas as pd


class Io:
    def __init__(self):
        DATA_DIR = os.path.join(os.path.dirname(__file__), "../../data")
        FILE_NAME = os.path.join(DATA_DIR, "grion_db.out")
        self.load(FILE_NAME)

    def load(self, file_name):
        self._df = pd.read_pickle(file_name)

    @property
    def df(self):
        return self._df


IO = Io()

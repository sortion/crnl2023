# -*- coding: utf-8 -*-

"""
Generic tools to manipulate data
"""
import numpy as np
import scipy.signal as ss
import timefreqtools as tft


def signalToTrig(sig, thresh, front=True, clean=True, check=True):
    """
    Find pulse in a continuous signal.
    """
    if front:
        (ind1,) = np.where((sig[:-1] < thresh) & (sig[1:] >= thresh))
        (ind2,) = np.where((sig[:-1] >= thresh) & (sig[1:] < thresh))
    else:
        (ind1,) = np.where((sig[:-1] > thresh) & (sig[1:] <= thresh))
        (ind2,) = np.where((sig[:-1] <= thresh) & (sig[1:] > thresh))
    if clean:
        if ind1.size >= 1 and ind2.size >= 1 and ind2[0] < ind1[0]:
            ind2 = ind2[1:]
        if ind1.size >= 1 and ind2.size >= 1 and ind1[-1] > ind2[-1]:
            ind1 = ind1[:-1]
        if ind1.size == 1 and ind2.size == 0:
            ind1 = np.array([], dtype=int)
        if ind1.size == 0 and ind2.size == 1:
            ind2 = np.array([], dtype=int)
    if check:
        assert ind1.size == ind2.size, "Not the same ind1 and ind2 size {} {}".format(
            ind1.size, ind2.size
        )
    return ind1, ind2


def stack_sig(
    sig, sr, list_times, time_window, squeeze_1D=True, return_used_indices=False
):
    """
    From a signal (1D or 2D array,time is first dim), get a stack of all window around list_times as a stack
    sr is the sampling rate

    output : stack of slices, 'list_times' index is the first dimension
    """
    if sig.ndim == 1:
        sig = sig[:, np.newaxis]

    # Convert times to indices
    index_window = [int(round(time_window[0] * sr)), int(round(time_window[1] * sr))]
    list_n = [int(round(tt * sr)) for tt in list_times]

    # Keep only times where the window is fully overlapping the signal
    list_n = [
        (ind, n)
        for ind, n in enumerate(list_n)
        if (n + index_window[0] >= 0) & (n + index_window[1] < sig.shape[0])
    ]
    if len(list_n) < len(list_times):
        print(
            f"{len(list_times)-len(list_n)} time windows were out of signals and not included"
        )

    # Make the stack
    if list_n:
        used_indices, list_n = [list(l) for l in list(zip(*list_n))]
        sig_slices = np.stack(
            [sig[nn + index_window[0] : nn + index_window[1], :] for nn in list_n]
        ).astype(sig.dtype)
    else:
        used_indices = []
        sig_slices = np.empty(
            (0, index_window[1] - index_window[0], sig.shape[1]), dtype=sig.dtype
        )

    if (sig.shape[1] == 1) and squeeze_1D:
        sig_slices = sig_slices[:, :, 0]

    if return_used_indices:
        return sig_slices, used_indices
    else:
        return sig_slices


def compute_filtered_sig(
    sig, sampling_rate, f_start, f_stop, axis=-1, target_sampling_rate=None, **kargs
):
    """
    Decimate and filter a signal along a given axis.
    Default target_sampling_rate: 10 * f_stop

    Output: filtered signal, new sampling rate
    """
    # sub sampling with decimate
    if target_sampling_rate is None:
        target_sampling_rate = min(10.0 * f_stop, sampling_rate)
    ratio = max(int(sampling_rate / target_sampling_rate), 1)
    if ratio > 1:
        sig2 = ss.decimate(sig, ratio, axis=axis)
    else:
        sig2 = sig
    new_sr = sampling_rate / ratio

    # filter signal
    sigf = tft.filter_signal(sig2, new_sr, f_start, f_stop)  # axis=axis, **kargs)

    return sigf, new_sr


def compute_hilbert_envelope(
    sig, sampling_rate, f_start, f_stop, t_start=0.0, axis=-1, **kargs
):
    """
    Compute the hilbert envelope between f_start and f_stop
    after decimating signal to kargs 'target_sampling_rate' (10* f_stop if not specified)

    Output: times, amplitudes, phases, envelope sampling_rate
    """

    # Decimate and filter signal
    sigf, env_sampling_rate = compute_filtered_sig(
        sig, sampling_rate, f_start, f_stop, axis=axis, **kargs
    )
    env_times = np.arange(sigf.shape[axis]) / env_sampling_rate + t_start

    # Pad signal and compute hilbert transform
    init_n = sigf.shape[axis]
    n = int(2 ** np.ceil(np.log(init_n) / np.log(2)))
    padded_shape = list(sigf.shape)
    padded_shape[axis] = n
    sigf_padded = np.zeros(padded_shape)
    if axis < 0:
        axis = sigf.ndim + axis
    sigf_padded[tuple([slice(None)] * axis + [slice(init_n), Ellipsis])] = sigf

    env_hilbert = np.take(ss.hilbert(sigf_padded, axis=axis), range(init_n), axis=axis)
    env_ampl = np.abs(env_hilbert)
    env_phase = np.angle(env_hilbert)

    return env_times, env_ampl, env_phase, env_sampling_rate


def test_stack_sig():
    # 1D test
    n = 5
    sr = 1000

    sig = np.zeros(sr * n)
    for i in range(n):
        ind_start = i * sr + 100
        ind_stop = i * sr + 200
        sig[ind_start:ind_stop] = 2

        ind_start = i * sr + 400
        ind_stop = i * sr + 500
        sig[ind_start:ind_stop] = 1

    import matplotlib.pyplot as plt

    list_times = [i + 0.1 for i in range(n)]
    time_window = [-0.5, 1]
    stack, used_inds = stack_sig(
        sig, sr, list_times, time_window=time_window, return_used_indices=True
    )
    tt = np.arange(stack.shape[1]) / sr + time_window[0]
    print("Used inds: ", used_inds)
    print("Stack shape: ", stack.shape)
    plt.plot(tt, stack.mean(axis=0))

    # 2D test
    n = 5
    sr = 1000

    sig = np.zeros((sr * n, 10))
    for i in range(n):
        ind_start = i * sr + 100
        ind_stop = i * sr + 200
        sig[ind_start:ind_stop, :] = 2

        ind_start = i * sr + 400
        ind_stop = i * sr + 500
        sig[ind_start:ind_stop, :] = 1

    import matplotlib.pyplot as plt

    list_times = [i + 0.1 for i in range(n)]
    time_window = [-0.5, 1]
    stack, used_inds = stack_sig(
        sig, sr, list_times, time_window=time_window, return_used_indices=True
    )
    tt = np.arange(stack.shape[1]) / sr + time_window[0]
    print("Used inds: ", used_inds)
    print("Stack shape: ", stack.shape)
    plt.imshow(
        stack.mean(axis=0).T,
        origin="lower",
        extent=(tt[0], tt[-1], -0.5, 9.5),
        interpolation="nearest",
        aspect="auto",
    )
    plt.colorbar()
    plt.show()


if __name__ == "__main__":
    test_stack_sig()

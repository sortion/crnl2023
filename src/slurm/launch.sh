#!/bin/bash
##SBATCH --job-name=Nom du Script
##SBATCH –output=fichier_de_sortie.txt
##SBATCH –error=sortie_erreur.err
##SBATCH –cpus-per-task=2 #a adapter
##SBATCH –mem=10G
srun --cpus-per-task=8 python3 --mem=10G /home/samuel.ortion/Projects/intership/src/slurm/launch_multithreaded_window_extraction.py &

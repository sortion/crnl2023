#!/bin/env python3
# -*- coding: utf8 -*-

"""
Extract raw and filtered signal data from the generated Grion et al. data pandas dataframe.
Signal are stacked as windows around -2s/+2s of each signal around the whisker touch timestamp.
Data is saved as a pandas dataframe.
"""

import os
from typing import List

import numpy as np
import pandas as pd
import typer
from tqdm.auto import tqdm

from extract import extract_around
from tools import compute_filtered_sig, compute_hilbert_envelope


def max_delta_t(grion_df, max=2):
    min_delta: float = None
    for _, row in grion_df.iterrows():
        LFP_times = row["LFP_times"]
        whisker_touch_time = row["touch_start"]
        if np.any(np.isnan(LFP_times)):
            continue
        min_time = LFP_times[0]
        max_time = LFP_times[-1]
        max_delta_time = min(
            whisker_touch_time - min_time, max_time - whisker_touch_time
        )
        if min_delta is None:
            min_delta = max_delta_time
        elif min_delta > max_delta_time:
            min_delta = max_delta_time
    return min(max_delta_time, max)


def construct_windows_around(grion_df, f_min, f_max, session="all"):
    """

    :return: Signal windows around touch start dataset
    :rtype: pandas.DataFrame
    """
    # Set delta_t to maximal values,
    # when all windows fits in the available data
    delta_t = max_delta_t(grion_df)
    if session != "all":
        grion_df = grion_df[grion_df["session"] == session]
    windows: List = []
    # grion_df = grion_df.iloc[:10]
    for _, row in tqdm(grion_df.iterrows()):
        LFP = row["LFP"]
        LFP_times = row["LFP_times"]
        LFP_sr = 2000
        whisker_touch_time = row["touch_start"]
        # Avoid NaNs
        if np.any(np.isnan(LFP_times)):
            continue
        # Extract raw LFP signal window
        LFP_window = extract_around(
            LFP, LFP_times, whisker_touch_time, int(LFP_sr * delta_t)
        )
        # Compute filtered signal
        LFP_filtered, LFP_filtered_sr = compute_filtered_sig(LFP, LFP_sr, f_min, f_max)
        LFP_filtered_times = np.linspace(LFP_times[0], LFP_times[-1], len(LFP_filtered))
        # Compute hilbert envelope
        env_times, env_ampl, env_phase, env_sampling_rate = compute_hilbert_envelope(
            LFP, LFP_sr, f_min, f_max, t_start=LFP_times[0]
        )
        LFP_filtered_window = extract_around(
            LFP_filtered,
            LFP_filtered_times,
            whisker_touch_time,
            int(delta_t * LFP_filtered_sr),
        )
        LFP_hilbert_window = extract_around(
            env_phase, env_times, whisker_touch_time, int(delta_t * env_sampling_rate)
        )
        if (
            LFP_window is not None
            and LFP_filtered_window is not None
            and LFP_hilbert_window is not None
        ):
            data = pd.Series(
                {
                    "session": row["session"],
                    # Avoid memory overflow
                    # "LFP": LFP_window,
                    # "LFP_times": np.linspace(-delta_t, delta_t, len(LFP_window)),
                    # "LFP_filtered": LFP_filtered_window,
                    # "LFP_filtered_times": np.linspace(
                    #     -delta_t, delta_t, len(LFP_filtered_window)
                    # ),
                    "LFP_hilbert": LFP_hilbert_window,
                    "LFP_hilbert_times": np.linspace(
                        -delta_t, delta_t, len(LFP_hilbert_window)
                    ),
                }
            )
            windows.append(data)
    if windows == []:
        raise ValueError("No windows found")
    dataset = pd.concat(windows, axis=1)
    return dataset


def cli(
    rat: int = 3,
    session: str = None,
    input_file: str = "./data/grion_db.out",
    output_file: str = None,
    f_min: int = 5,
    f_max: int = 12,
):
    grion_df = pd.read_pickle(input_file)
    grion_df_filtered = grion_df[grion_df["rat"] == rat]
    if session is not None:
        dataset: pd.DataFrame = construct_windows_around(
            grion_df_filtered, f_min, f_max, session=session
        ).T
        if output_file is None:
            output_file = os.path.join(
                "data", f"grion_lfp_windows_r{rat}_s{session}_df.out"
            )
        dataset.to_pickle(output_file)
    else:
        dataset: pd.DataFrame = construct_windows_around(
            grion_df_filtered, f_min, f_max
        ).T
        if output_file is None:
            output_file = os.path.join("data", f"grion_lfp_windows_r{rat}_df.out")
        dataset.to_pickle(output_file)


def main():
    typer.run(cli)


if __name__ == "__main__":
    main()

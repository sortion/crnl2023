#!/usr/bin/env python3
"""
Extract whisking windows around texture contacts for all session of a rat
"""


import whisking


def main():
    RAT = "R3"
    DATA_DIR = "data/1536795"
    sessions = whisking.get_whisking_session(RAT, DATA_DIR)
    WIDTH = 100
    for session in sessions:
        print(f"Processing session {session}")
        whisking_angles, whisking_timestamps = whisking.get_whisking_data(
            RAT, session, DATA_DIR
        )
        contact_timestamps = whisking.get_contact_timestamps(RAT, session, DATA_DIR)
        windows = whisking.get_all_data_around(
            whisking_angles, whisking_timestamps, contact_timestamps, width=WIDTH
        )

        for window in windows:
            print(window.shape)


if __name__ == "__main__":
    main()

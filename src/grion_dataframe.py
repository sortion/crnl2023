#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
Extract data from Grion et al. 2016, for a selection of rat and save it as a dataframe.
Does not perform any data transformation.

Usage:
    ./grion_dataframe.py --datadir ./data/1536795 --outfile ./data/grion_db.out


"""

import os

import numpy as np
import pandas as pd
import scipy.io as sio
import typer
from scipy.interpolate import interp1d


def load_mat_to_df(filename, varname):
    """
    Read a file of type "Lfp_sess*.mat"

    :param filename: the .mat file path
    :param varname: the .mat data variable key
    :return: a dataframe containing the data from the matlab data file
    :rtype: pd.DataFrame
    """
    lfp = sio.loadmat(filename, variable_names=varname)
    lfp = lfp[varname][0, 0]
    lfp_dict = {}
    for key in lfp.dtype.fields.keys():
        lfp_dict[key] = lfp[key]
        if lfp_dict[key].size == 1:
            lfp_dict[key] = lfp_dict[key][0, 0]
        else:
            lfp_dict[key] = lfp_dict[key].flatten()
    return pd.DataFrame(lfp_dict)


def cli(datadir: str = "./data/1536795", outfile: str = "./data/grion_db.out"):
    all_trial_series = []
    for rat_number in [3, 4, 5, 6]:
        # Load global rat data
        beha_df = load_mat_to_df(
            os.path.join(datadir, "R" + str(rat_number), "Behavior.mat"), "Beha"
        )
        whisking_df = load_mat_to_df(
            os.path.join(datadir, "R" + str(rat_number) + "/Whisking.mat"), "Whisking"
        )
        whisking_df = whisking_df[whisking_df["framestarthispeed"] != 0]
        for session in [1, 2, 3, 4, 5]:
            # Load session data
            try:
                lfp_df = load_mat_to_df(
                    os.path.join(
                        datadir,
                        "R" + str(rat_number) + "_lfp/Lfp_sess" + str(session) + ".mat",
                    ),
                    "Lfp",
                )
            except FileNotFoundError:
                continue

            # Get trial data
            trial_numbs = whisking_df.loc[
                (whisking_df["rat"] == rat_number)
                & (whisking_df["session"] == session),
                "trialnumb",
            ]
            session_beha = beha_df.loc[
                (beha_df["rat"] == rat_number) & (beha_df["session"] == session)
            ]
            trial_LFP = lfp_df.loc[
                (lfp_df["rat"] == rat_number) & (lfp_df["session"] == session)
            ]
            # session_whisk = whisking_df.loc[
            #     (whisking_df["rat"] == rat_number) & (whisking_df["session"] == session)
            # ]

            # Interpolate signal because of missing timestamps
            tt = trial_LFP["timestamps"].values
            LFP = trial_LFP["voltage"].values
            sr = trial_LFP["sr"].iloc[0]
            interp_LFP = interp1d(tt, LFP)
            tt = np.arange(tt[0], tt[-1], 1.0 / sr)  # New tt without missing times
            LFP = interp_LFP(tt)  # New LFP with linear interpolation

            for trial_numb in trial_numbs:
                trial_whisking = whisking_df.loc[
                    (whisking_df["rat"] == rat_number)
                    & (whisking_df["session"] == session)
                    & (whisking_df["trialnumb"] == trial_numb)
                ].iloc[0]
                wh_time = trial_whisking["timestampframestarthispeed"]

                wh_time += (
                    1.0
                    * np.arange(
                        trial_whisking["startframe"], trial_whisking["stopframe"] + 1
                    )
                    - trial_whisking["framestarthispeed"]
                ) / trial_whisking["fps"]
                wh_angles = trial_whisking["meanangle"]

                trial_beha = session_beha[session_beha["trialnumb"] == trial_numb]

                trial_serie = pd.Series()
                trial_serie["rat"] = rat_number
                trial_serie["session"] = session
                trial_serie["trialnumb"] = trial_numb
                trial_serie["touch_start"] = trial_beha["startcontact"].values[0]
                trial_serie["touch_dur"] = trial_beha["stopcontact"].values[0]
                trial_serie["whisking_times"] = wh_time
                trial_serie["whisking"] = wh_angles[:, 0]
                trial_serie["LFP"] = LFP
                trial_serie["LFP_times"] = tt
                trial_serie["success"] = trial_beha["correctness"].values[0]
                trial_serie["texture"] = trial_beha["texture"].values[0]
                trial_serie["rewardtime"] = trial_beha["rewardtime"].values[0]
                all_trial_series.append(trial_serie)

    full_df = pd.concat(all_trial_series, axis=1).T
    full_df.to_pickle(outfile)


def main():
    typer.run(cli)


if __name__ == "__main__":
    main()

"""
Load dataframe with whisking and LFP hilbert and search for phase precession or phase locking
"""

from scipy.interpolate import interp1d
from scipy import *
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.neighbors import KernelDensity

def plot_aligned_phasediff(df,
                            align_normtime=0,
                            time_win=[-0.3,0.5],
                            sr=1000.,
                            align_phase_at_time=None, # time, in time_win where phasediff is considered as being 0 for all trials
                            meas_min=-5., # ar which phase diffs is the kernel density computed
                            meas_max=5.,
                            meas_num=100,
                            ):
    """
    Take all phase diff
    
    align_normtime is 0 for contact start and 1 for contact stop, linearly interpolated for any other value
    
    time_win is in real time units (window around align_normtime where signals are averaged)
    
    """

    t_aligned=r_[arange(0,time_win[0],-1./sr)[::-1],arange(0,time_win[1],1./sr)[1:]]
    aligned_phasediff=pd.DataFrame(index=df.index,columns=t_aligned)
    
    for i,ts in df.iterrows():
        if ts['HLFP'].size!=ts['Hwhisk'].size:
            print "Skip rat/session/number : ",ts['rat'],ts['session'],ts['trialnumb']
            continue
        phase_diff=mod(angle(ts['HLFP'])-angle(ts['Hwhisk'])+pi,2*pi)-pi
        # ts keys of interest: touch_start,touch_dur,times,HLFP,Hwhisk
        align_time=ts['touch_start']+align_normtime*ts['touch_dur']
        tt=ts['times']-align_time
        interp_phasediff=interp1d(tt,phase_diff,kind='nearest',assume_sorted=True,bounds_error=False) # 'nearest' is necessary because of the discontinuity of phase_diff which is problematic for linear
        aligned_phasediff.loc[i,:]=interp_phasediff(t_aligned)

    if align_phase_at_time is not None:
        ind=abs(t_aligned-align_phase_at_time).argmin()
        aligned_phasediff=aligned_phasediff[aligned_phasediff.iloc[:,ind].notnull()]
        aligned_phasediff=aligned_phasediff.sub(aligned_phasediff.iloc[:,ind],axis=0)
        aligned_phasediff=mod(aligned_phasediff+pi,2*pi)-pi

    fig=plt.figure()
    ax=fig.add_subplot(111)
    ndat=aligned_phasediff.count()
    ax.plot(ndat.index,ndat.values)
    
    mm=linspace(meas_min,meas_max,meas_num)
    phasediff_map=pd.DataFrame(index=mm,columns=t_aligned)
    for t in t_aligned:
        data=r_[aligned_phasediff[t].dropna(),aligned_phasediff[t].dropna()+2*pi,aligned_phasediff[t].dropna()-2*pi]
        if data.size>0:
            kernel=KernelDensity().fit(data[:,newaxis])
            phasediff_map[t]=exp(kernel.score_samples(mm[:,newaxis]))

    fig=plt.figure()
    ax=fig.add_subplot(111)
    img=ax.imshow(phasediff_map.fillna(0.),
                        interpolation='nearest', 
                        extent=(t_aligned[0],t_aligned[-1],mm[0],mm[-1]),
                        origin ='lower' ,
                        aspect = 'auto',
                        cmap='hot',
                        )
    img.set_clim(0.,.15)

    
if __name__=="__main__":

    import pickle

    fid=open("DataFrame_TrialHilbert_whisker1.out","rb")
    full_df=pickle.load(fid)
    fid.close()

    #~ full_df=full_df[full_df['success']==0]
    full_df=full_df[full_df['rat']==5]

    plot_aligned_phasediff(full_df,
                                        align_normtime=0,
                                        time_win=[-.3,.5],
                                        align_phase_at_time=0.1)
                                        
    plot_aligned_phasediff(full_df,
                                        align_normtime=0,
                                        time_win=[-.5,.5],
                                        align_phase_at_time=-0.2)

    plot_aligned_phasediff(full_df,
                                        align_normtime=1,
                                        time_win=[-.5,.2],
                                        align_phase_at_time=-0.2)

    plt.show()


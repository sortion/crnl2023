"""
Fill a dataframe with, for all trials, metadata, time and aligned hilbert transform of whisking and lfp
"""

from scipy.io import loadmat
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import hilbert,cheby1,filtfilt,iirfilter,resample
from scipy.interpolate import interp1d

def load_mat_to_df(filename,varname):
    
    """
    Read a file of type "Lfp_sess*.mat"
    """
    lfp=loadmat(filename,variable_names=varname)
    lfp=lfp[varname][0,0]
    lfp_dict={}
    print "Loaded keys : ",
    for key in lfp.dtype.fields.keys():
        print key,
        lfp_dict[key]=lfp[key]
        if lfp_dict[key].size==1:
            lfp_dict[key]=lfp_dict[key][0,0]
        else:
            lfp_dict[key]=lfp_dict[key].flatten()
            print lfp_dict[key].shape,
    print

    return pd.DataFrame(lfp_dict)

def compute_hilbert(sig,sr,f_start=4.,f_stop=10.,output_sr=None):
    """
    sig: dat, sr: samplingrate
    No change in sampling rate for now
    output_sr is sampling rate of output signals (same as sr if output_sr is None)
    """
    
    # Decimate signal
    if output_sr is not None:
        ratio = int(1.*sr/output_sr)
        if ratio>1:
            b, a = cheby1(5, 0.05, 0.8 / ratio)
            y = filtfilt(b, a, sig, axis=-1)
            sl = [slice(None)] * y.ndim
            sl[-1] = slice(None, None, ratio)
            sig2=y[sl]
            #~ sig2 = decimate(sig, ratio) # 
        else:
            sig2 = sig
        sr=sr/ratio
        env_times = np.arange(sig2.size)/sr
    else:
        sig2=sig
    
    env_times = 1.*np.arange(sig2.size)/sr
    
    # Filter signal (taken from timefreqtools)
    Wn = [f_start/(sr/2.), f_stop/(sr/2.) ]
    coeffs = iirfilter(N=3, Wn=Wn, btype = 'bandpass', analog=False, ftype='butter', output='ba')
    b, a = coeffs
    sigf=filtfilt(b, a, sig2)            

    n=int(2**np.ceil(np.log(sigf.size)/np.log(2)))
    sigf_padded = np.zeros(n, dtype = sigf.dtype)
    sigf_padded[:sigf.size] = sigf
    env=hilbert(sigf_padded)[:sigf.size]

    if output_sr is not None:
        return env,sigf,env_times
    else:
        return env,sigf


if __name__=="__main__":

    plot_all=False

    all_trial_series=[]
    for rat_number in [3,4,5,6]:

        # Load global rat data
        beha_df=load_mat_to_df("R"+str(rat_number)+"/Behavior.mat",'Beha')
        print beha_df.columns

        whisking_df=load_mat_to_df("R"+str(rat_number)+"/Whisking.mat",'Whisking')
        print whisking_df.columns

        print "Rat :",rat_number,(whisking_df['framestarthispeed']==0).sum()
        whisking_df=whisking_df[whisking_df['framestarthispeed']!=0]

        for session in [1,2,3,4,5]:
            try:
                plt.show()
            except:
                pass
            
            # Load session data
            try:
                lfp_df=load_mat_to_df("R"+str(rat_number)+"_lfp/Lfp_sess"+str(session)+".mat",'Lfp')
                print "R"+str(rat_number)+"_lfp/Lfp_sess"+str(session)+".mat loaded"
            except:
                print "R"+str(rat_number)+"_lfp/Lfp_sess"+str(session)+".mat was not loaded (do not exist???)"
                continue

            # Get trial data
            trial_numbs=whisking_df.loc[(whisking_df['rat']==rat_number)&
                                                        (whisking_df['session']==session),'trialnumb']
            session_beha=beha_df.loc[(beha_df['rat']==rat_number)&
                                            (beha_df['session']==session)]
            trial_LFP=lfp_df.loc[(lfp_df['rat']==rat_number)&
                                    (lfp_df['session']==session)]
            session_whisk=whisking_df.loc[(whisking_df['rat']==rat_number)&
                                            (whisking_df['session']==session)]

            
            # Interpolate signal because of missing timestamps 
            tt=trial_LFP['timestamps'].values
            LFP=trial_LFP['voltage'].values
            sr=trial_LFP['sr'].iloc[0]
            interp_LFP=interp1d(tt,LFP)
            tt=np.arange(tt[0],tt[-1],1./sr) # New tt without missing times
            LFP=interp_LFP(tt) # New LFP with linear interpolation

            # Compute LFP Hilbert 
            HLFP,filtLFP=compute_hilbert(LFP,sr,f_start=4.,f_stop=12.)

            # Interpolate Hilbert to later align with whisking data (and because some timestamps are missing)
            interp_HLFP=interp1d(tt,HLFP)
            

            if plot_all:
                fig=plt.figure()
                ax1=fig.add_subplot(211)

                ax2=fig.add_subplot(212,sharex=ax1)
                ax2.plot(tt,LFP)

                fig2=plt.figure()
                ax3=fig2.add_subplot(211,sharex=ax1)
                ax4=fig2.add_subplot(212,sharex=ax1)
                        
                # Compare filtered sig, full Hilbert and interpolated Hilbert
                ax4.plot(tt,filtLFP,'k')
                ax4.plot(tt,np.abs(HLFP)*np.cos(np.angle(HLFP)),'g')
                srd=1000. # Whisking frame sampling rate (assumed to be always 1000 fps)
                ttd=np.arange(tt[0],tt[-1],1./srd)
                HLFPr=interp_HLFP(ttd)
                ax4.plot(ttd,np.abs(HLFPr)*np.cos(np.angle(HLFPr)),'m')

                print "Trials: ",trial_numbs.values

                print "Trial behavior: "
                print session_beha
                print 

                print "Trial whisk data: "
                print session_whisk
                print 


            for trial_numb in trial_numbs:

                trial_whisking=whisking_df.loc[(whisking_df['rat']==rat_number)&
                                                            (whisking_df['session']==session)&
                                                            (whisking_df['trialnumb']==trial_numb)].iloc[0]
                wh_time=trial_whisking['timestampframestarthispeed']
                wh_time+=(1.*np.arange(trial_whisking['startframe'],trial_whisking['stopframe']+1)-trial_whisking['framestarthispeed'])/trial_whisking['fps']
                wh_angles=trial_whisking['meanangle']

                trial_beha=session_beha[session_beha['trialnumb']==trial_numb]

                Hwhisk,filtwhisk=compute_hilbert(wh_angles[:,0],trial_whisking['fps'],f_start=4.,f_stop=12.)
                if wh_time[0]<tt[-1]:
                    trialHLFP=interp_HLFP(wh_time)
                else: # !!!!!!!!!!! CET EXCEPTION A ETE MISE POUR LE RAT 4 MAIS CE N'EST PAS TRES NORMAL IL DOIT Y AVOIR UN PB SUR LE CALCUL DES TEMPS POUR CE RAT, A VERIFIER
                    print "Weird wh_time"
                    trialHLFP=np.empty(wh_time.size)
                    trialHLFP[:]=np.nan

                if plot_all:
                    ax1.plot(wh_time,wh_angles[:,0])
                    ax1.plot(wh_time,wh_angles[:,1])
                    ax1.axvline(trial_whisking['timestampframestarthispeed'],color='r')

                    ax2.axvline(trial_beha['startcontact'].values)
                    ax2.axvline(trial_beha['startcontact'].values+trial_beha['stopcontact'].values)

                    ax3.plot(wh_time,filtwhisk,'k')
                    ax3.plot(wh_time,np.abs(Hwhisk)*np.cos(np.angle(Hwhisk)),'g')
                    ax4.plot(wh_time,trialHLFP,'b')

                trial_serie=pd.Series()
                trial_serie['rat']=rat_number
                trial_serie['session']=session
                trial_serie['trialnumb']=trial_numb
                trial_serie['touch_start']=trial_beha['startcontact'].values[0]
                trial_serie['touch_dur']=trial_beha['stopcontact'].values[0]
                trial_serie['times']=wh_time
                trial_serie['HLFP']=trialHLFP
                trial_serie['Hwhisk']=Hwhisk
                trial_serie['success']=trial_beha['correctness'].values[0]
                trial_serie['texture']=trial_beha['texture'].values[0]
                trial_serie['rewardtime']=trial_beha['rewardtime'].values[0]
                all_trial_series.append(trial_serie)

    full_df=pd.concat(all_trial_series,axis=1).T
    full_df.to_pickle("DataFrame_TrialHilbert_whisker1.out")

    plt.show()

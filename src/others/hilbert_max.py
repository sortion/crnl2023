"""
Max's code chunk for computing the Hilbert transform of a signal and extracting the phase
"""
# Compute its Hilbert transform on a selected frequency band
fs = sr
_, _, env_phase, _ = compute_hilbert_envelope(
    sig, fs, f_low, f_high, target_sampling_rate=sr
)

# Cut and stack by trials
t_align = dataio.get_trig_times(key + session, align_on)
phase_stacks = stack_sig(env_phase, sr, t_align, time_window)

# Rayleigh testing the uniformity
rayleigh = rayleightest(phase_stacks, axis=0)
z = np.array([[st.norm.ppf(r) for r in rayleigh]])

plt.plot(time, z)
plt.xlabel("Time [s]", fontsize=14)
plt.ylabel("Z-score", fontsize=14)

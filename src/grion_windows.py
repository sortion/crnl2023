#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""
Extract raw and filtered signal data from the generated Grion et al. data pandas dataframe.
Signal are stacked as windows around -2s/+2s of each signal around the whisker touch timestamp.
Data is saved as a xarray dataset.
"""

from typing import List

import numpy as np
import pandas as pd
import xarray as xr

from extract import extract_around
from tools import compute_filtered_sig


def construct_windows_around(grion_df, f_min, f_max):
    """

    :return: Signal windows around touch start dataset
    :rtype: xarray.DataSet
    """
    delta_t = 0.5
    windows: List[xr.DataArray] = []
    for _, row in grion_df.iterrows():
        LFP = row["LFP"]
        LFP_times = row["LFP_times"]
        LFP_sr = 2000
        whisking = row["whisking"]
        whisking_times = row["whisking_times"]
        whisker_touch_time = row["touch_start"]
        # Avoid NaNs
        if np.any(np.isnan(whisking_times)) or np.any(np.isnan(LFP_times)):
            continue
        # Compute filtered signal
        LFP_filtered, LFP_filtered_sr = compute_filtered_sig(LFP, LFP_sr, f_min, f_max)
        LFP_window = extract_around(
            LFP, LFP_times, whisker_touch_time, int(delta_t * 2000)
        )
        whisking_filtered, whisking_filtered_sr = compute_filtered_sig(
            whisking,
        )  # FIXME: unterminated input
        # whisking_window = extract_around(
        #     whisking, whisking_times, whisker_touch_time, int(delta_t * 1000)
        # )
        # # TODO: fix whisking window extraction, returns None, why?
        if whisking_window is not None and LFP_window is not None:
            data_array: xr.DataArray = xr.DataArray(
                dims=["time", "channel"],
                coords={
                    "time": np.linspace(-2, 2, len(LFP_window)),
                    "channel": ["LFP", "whisking"],
                    "rat": row["rat"],
                },
                data=[LFP_window, whisking_window],
            )
            windows.append(data_array)
    if windows == []:
        raise ValueError("No windows found")
    dataset = xr.concat(windows, dim="rat")
    return dataset


def main():
    PICKLE_FILE = "./data/grion_db.out"
    F_MIN = 5
    F_MAX = 12
    grion_df = pd.read_pickle(PICKLE_FILE)
    dataset: xr.DataSet = construct_windows_around(grion_df, F_MIN, F_MAX)
    dataset.to_netcdf("./data/grion_windows.nc")


if __name__ == "__main__":
    main()

#!/usr/bin/env python3
"""
Extract whisking signal phases
"""

import os

import numpy as np
import scipy.io as sio


def get_whisking_session(rat, data_dir):
    filename = os.path.join(data_dir, rat, "Whisking.mat")
    data = sio.loadmat(filename)["Whisking"]
    sessions = data["session"][0][0]
    return np.unique(sessions)


def get_whisking_data(rat, session, data_dir):
    """
    Load the whisking data for a given rat and session
    """
    # Read mat file
    filename = os.path.join(data_dir, rat, "Whisking.mat")
    data = sio.loadmat(filename)["Whisking"]
    angles = data["meanangle"][0][0][0]
    timestamps = data["timestampframestarthispeed"][0][0][0]
    sessions = data["session"][0][0][0]
    # Get the data for the given session
    angles = angles[sessions == session]
    timestamps = timestamps[sessions == session]
    return angles, timestamps


def get_contact_timestamps(rat, session, data_dir):
    """
    Load rat texture contact timestamps

    rat -- (str) the rat name (ex: "R1")
    session -- (int) the session number (ex: 1)
    data_dir -- (str) the path to the data directory
    """
    filename = os.path.join(data_dir, rat, f"Behavior.mat")
    data = sio.loadmat(filename)["Beha"]
    contact_start_timestamps = data["startcontact"][0][0][0]
    contact_session = data["session"][0][0][0]
    contact_start_timestamps = contact_start_timestamps[contact_session == session]
    return contact_start_timestamps


def get_lfp_data(rat, session, data_dir):
    """
    Load the LFP data for a given rat and session

    rat -- (str) the rat name (ex: "R1")
    session -- (int) the session number (ex: 1)
    data_dir -- (str) the path to the data directory
    """
    # Read mat file
    filename = os.path.join(data_dir, f"{rat}_lfp", f"Lfp_sess{session}.mat")
    data = sio.loadmat(filename)["LFP"]
    voltage = data["voltage"][0][0][0]
    timestamps = data["timestamps"][0][0][0]
    return voltage, timestamps


def get_data_around(data, data_timestamps, target_timestamp, width=100):
    """
    Get the data around a given timestamp

    data -- the data to extract from
    data_timestamps -- the timestamps of the data
    target_timestamp -- the timestamp to extract around
    """
    if data is None:
        return None
    closest_frame = np.argmin(np.abs(data_timestamps - target_timestamp))
    if (
        closest_frame == -1
        or closest_frame + width >= len(data)
        or closest_frame - width < 0
    ):
        return None
    return data[closest_frame - width : closest_frame + width]


def get_all_data_around(data, data_timestamps, target_timestamps, width=100):
    """
    Get the data around a list of timestamps
    """
    data_around = []
    for target_timestamp in target_timestamps:
        data = get_data_around(data, data_timestamps, target_timestamp, width=width)
        if data is not None:
            data_around.append(data)
    return data_around

# ~/Projects/intership/.venv/crnl-clust/bin/python3
# -*- coding: utf8 -*-

"""
Extract raw and filtered signal data from the generated Grion et al. data pandas dataframe.
Signal are stacked as windows around -2s/+2s of each signal around the whisker touch timestamp.
Data is saved as a pandas dataframe.
"""

from typing import List

import numpy as np
import pandas as pd
import typer
from debugprint import Debug
from tqdm.auto import tqdm

from extract import extract_around
from tools import compute_filtered_sig, compute_hilbert_envelope

debug = Debug("grion_whisking_windows:windows_around")


def max_delta_t(grion_df, max=2):
    """
    Retrieve the maximal delta_t for which all windows fits in the available data.
    """
    max_delta_t = float("inf")
    for _, row in grion_df.iterrows():
        whisking_times = row["whisking_times"]
        whisker_touch_time = row["touch_start"]
        if np.any(np.isnan(whisking_times)):
            continue
        if (
            whisking_times[0] > whisker_touch_time
            or whisker_touch_time > whisking_times[-1]
        ):
            continue
        delta_t_left = whisker_touch_time - whisking_times[0]
        delta_t_right = whisking_times[-1] - whisker_touch_time
        if delta_t_left < max_delta_t:
            max_delta_t = delta_t_left
        if delta_t_right < max_delta_t:
            max_delta_t = delta_t_right
    return max_delta_t


def largest_window(grion_df, max=2):
    """
    Retrieve the largest window width possible as a tuple (left, right)
    """
    min_left: float = float("inf")
    min_right: float = float("inf")
    for _, row in grion_df.iterrows():
        whisking_times = row["whisking_times"]
        whisker_touch_time = row["touch_start"]
        if np.any(np.isnan(whisking_times)):
            continue
        if (
            whisking_times[0] > whisker_touch_time
            or whisker_touch_time > whisking_times[-1]
        ):
            continue
        delta_t_left = whisker_touch_time - whisking_times[0]
        delta_t_right = whisking_times[-1] - whisker_touch_time
        if delta_t_left < min_left:
            min_left = delta_t_left
        if delta_t_right < min_right:
            min_right = delta_t_right
    min_left = min(min_left, max)
    min_right = min(min_right, max)
    return (min_left, min_right)


def construct_windows_around(grion_df, f_min, f_max):
    """

    :return: Signal windows around touch start dataset
    :rtype: pandas.DataFrame
    """

    # Set delta_t to maximal values,
    # when all windows fits in the available data
    time_window = largest_window(grion_df)

    def scale_window(sample_rate):
        return (
            int(sample_rate * time_window[0]),
            int(sample_rate * time_window[1]),
        )

    # delta_t = 0.0075
    windows: List = []
    for _, row in tqdm(grion_df.iterrows()):
        whisking = row["whisking"]
        # print(whisking)
        whisking_times = row["whisking_times"]
        whisking_sr = 1000
        whisker_touch_time = row["touch_start"]
        if (
            whisking_times[0] > whisker_touch_time
            or whisker_touch_time > whisking_times[-1]
        ):
            continue
        if np.any(np.isnan(whisking_times)):
            continue

        # Extract raw whisking signal window
        whisking_window = extract_around(
            whisking, whisking_times, whisker_touch_time, scale_window(whisking_sr)
        )
        debug(f"Whisking window len: {len(whisking_window)}")
        # Compute filtered signal
        whisking_filtered, whisking_filtered_sr = compute_filtered_sig(
            whisking, whisking_sr, f_min, f_max
        )
        whisking_filtered_times = np.linspace(
            whisking_times[0], whisking_times[-1], len(whisking_filtered)
        )
        whisking_filtered_timerange = (
            whisking_filtered_times[-1] - whisking_filtered_times[0]
        )
        debug(f"Whisking filtered timerange: {whisking_filtered_timerange}")
        # Compute hilbert envelope
        env_times, env_ampl, env_phase, env_sampling_rate = compute_hilbert_envelope(
            whisking, whisking_sr, f_min, f_max, t_start=whisking_times[0]
        )
        debug(f"Whisking hilbert timerange: {env_times[-1] - env_times[0]}")
        whisking_filtered_window = extract_around(
            whisking_filtered,
            whisking_filtered_times,
            whisker_touch_time,
            scale_window(whisking_filtered_sr),
        )
        debug(
            f"Whisking filtered window len: {len(whisking_filtered_window)}"
        ) if whisking_filtered_window is not None else debug(
            "Whisking filtered window is None"
        )
        debug(f"env timerange: {env_times[-1] - env_times[0]}")
        whisking_hilbert_window = extract_around(
            env_phase, env_times, whisker_touch_time, scale_window(env_sampling_rate)
        )
        debug(
            f"Whisking hilbert window len: {len(whisking_hilbert_window)}"
        ) if whisking_hilbert_window is not None else debug(
            "Whisking hilbert window is None"
        )
        if (
            whisking_window is not None
            and whisking_filtered_window is not None
            and whisking_hilbert_window is not None
        ):
            data = pd.Series(
                {
                    "whisking": whisking_window,
                    "whisking_times": np.linspace(
                        -time_window[0], time_window[1], len(whisking_window)
                    ),
                    "whisking_filtered": whisking_filtered_window,
                    "whisking_filtered_times": np.linspace(
                        -time_window[0], time_window[1], len(whisking_filtered_window)
                    ),
                    "whisking_hilbert": whisking_hilbert_window,
                    "whisking_hilbert_times": np.linspace(
                        -time_window[0], time_window[1], len(whisking_hilbert_window)
                    ),
                }
            )
            # print(whisking_window)
            windows.append(data)
    if windows == []:
        raise ValueError("No windows found")
    dataset = pd.concat(windows, axis=1).T
    return dataset


def cli(rat: int = 3):
    PICKLE_FILE = "./data/grion_db.out"
    F_MIN = 1
    F_MAX = 15
    grion_df = pd.read_pickle(PICKLE_FILE)
    grion_df_filtered = grion_df[grion_df["rat"] == rat]
    dataset: pd.DataFrame = construct_windows_around(grion_df_filtered, F_MIN, F_MAX)
    dataset.to_pickle(f"./data/grion_whisking_windows_r{rat}_df.out")


def main():
    typer.run(cli)


if __name__ == "__main__":
    main()

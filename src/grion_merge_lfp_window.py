# ~/Projects/intership/.venv/crnl-clust/bin/python3
# -*- coding: utf8 -*-
"""
Merge LFP windows of a selection of rat, precomputed using gion_lfp_window.py --rat {rat}
"""

from typing import List

import pandas as pd
import typer


def cli(
    output_file: str = "./data/grion_windows_2s_5-12Hz_merged.out",
    rats: List[str] = [3, 4, 5, 6],
) -> None:
    """
    Merge LFP windows of selection of rat, precomputed using gion_lfp_window.py --rat {rat}
    """
    # Load data
    lfp_hilbert = []
    lfp_times = []
    for rat in rats:
        df = pd.read_pickle(f"./data/grion_lfp_windows_r{rat}_df.out")
        lfp_hilbert += df["LFP_hilbert"].to_list()
        lfp_times += df["LFP_hilbert_times"].to_list()
    df = pd.DataFrame({"LFP_hilbert": lfp_hilbert, "LFP_hilbert_times": lfp_times})
    df.to_pickle(output_file)


if __name__ == "__main__":
    typer.run(cli)

"""
Fill a dataframe with, for all trials, metadata, time and aligned hilbert transform of whisking and lfp

updated to python3
"""

import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
from scipy.io import loadmat
from scipy.signal import cheby1, filtfilt, hilbert, iirfilter


def load_mat_to_df(filename, varname):
    """
    Read a file of type "Lfp_sess*.mat"
    """
    lfp = loadmat(filename, variable_names=varname)
    lfp = lfp[varname][0, 0]
    lfp_dict = {}
    for key in lfp.dtype.fields.keys():
        lfp_dict[key] = lfp[key]
        if lfp_dict[key].size == 1:
            lfp_dict[key] = lfp_dict[key][0, 0]
        else:
            lfp_dict[key] = lfp_dict[key].flatten()
    return pd.DataFrame(lfp_dict)


def compute_hilbert(sig, sr, f_start=4.0, f_stop=10.0, output_sr=None):
    """
    sig: dat, sr: samplingrate
    No change in sampling rate for now
    output_sr is sampling rate of output signals (same as sr if output_sr is None)
    """

    # Decimate signal
    if output_sr is not None:
        ratio = int(1.0 * sr / output_sr)
        if ratio > 1:
            b, a = cheby1(5, 0.05, 0.8 / ratio)
            y = filtfilt(b, a, sig, axis=-1)
            sl = [slice(None)] * y.ndim
            sl[-1] = slice(None, None, ratio)
            sig2 = y[sl]
            # ~ sig2 = decimate(sig, ratio) #
        else:
            sig2 = sig
        sr = sr / ratio
        env_times = np.arange(sig2.size) / sr
    else:
        sig2 = sig

    env_times = 1.0 * np.arange(sig2.size) / sr

    # Filter signal (taken from timefreqtools)
    Wn = [f_start / (sr / 2.0), f_stop / (sr / 2.0)]
    coeffs = iirfilter(
        N=3, Wn=Wn, btype="bandpass", analog=False, ftype="butter", output="ba"
    )
    b, a = coeffs
    sigf = filtfilt(b, a, sig2)

    n = int(2 ** np.ceil(np.log(sigf.size) / np.log(2)))
    sigf_padded = np.zeros(n, dtype=sigf.dtype)
    sigf_padded[: sigf.size] = sigf
    env = hilbert(sigf_padded)[: sigf.size]

    if output_sr is not None:
        return env, sigf, env_times
    else:
        return env, sigf


def main():
    all_trial_series = []
    for rat_number in [3, 4, 5, 6]:
        # Load global rat data
        beha_df = load_mat_to_df("R" + str(rat_number) + "/Behavior.mat", "Beha")
        whisking_df = load_mat_to_df(
            "R" + str(rat_number) + "/Whisking.mat", "Whisking"
        )
        whisking_df = whisking_df[whisking_df["framestarthispeed"] != 0]
        for session in [1, 2, 3, 4, 5]:
            # Load session data
            try:
                lfp_df = load_mat_to_df(
                    "R" + str(rat_number) + "_lfp/Lfp_sess" + str(session) + ".mat",
                    "Lfp",
                )
            except FileNotFoundError:
                continue

            # Get trial data
            trial_numbs = whisking_df.loc[
                (whisking_df["rat"] == rat_number)
                & (whisking_df["session"] == session),
                "trialnumb",
            ]
            session_beha = beha_df.loc[
                (beha_df["rat"] == rat_number) & (beha_df["session"] == session)
            ]
            trial_LFP = lfp_df.loc[
                (lfp_df["rat"] == rat_number) & (lfp_df["session"] == session)
            ]
            # session_whisk = whisking_df.loc[
            #     (whisking_df["rat"] == rat_number) & (whisking_df["session"] == session)
            # ]

            # Interpolate signal because of missing timestamps
            tt = trial_LFP["timestamps"].values
            LFP = trial_LFP["voltage"].values
            sr = trial_LFP["sr"].iloc[0]
            interp_LFP = interp1d(tt, LFP)
            tt = np.arange(tt[0], tt[-1], 1.0 / sr)  # New tt without missing times
            LFP = interp_LFP(tt)  # New LFP with linear interpolation

            # Compute LFP Hilbert
            HLFP, filtLFP = compute_hilbert(LFP, sr, f_start=4.0, f_stop=12.0)

            # Interpolate Hilbert to later align with whisking data (and because some timestamps are missing)
            interp_HLFP = interp1d(tt, HLFP)

            for trial_numb in trial_numbs:
                trial_whisking = whisking_df.loc[
                    (whisking_df["rat"] == rat_number)
                    & (whisking_df["session"] == session)
                    & (whisking_df["trialnumb"] == trial_numb)
                ].iloc[0]
                wh_time = trial_whisking["timestampframestarthispeed"]
                wh_time += (
                    1.0
                    * np.arange(
                        trial_whisking["startframe"], trial_whisking["stopframe"] + 1
                    )
                    - trial_whisking["framestarthispeed"]
                ) / trial_whisking["fps"]
                wh_angles = trial_whisking["meanangle"]

                trial_beha = session_beha[session_beha["trialnumb"] == trial_numb]

                Hwhisk, filtwhisk = compute_hilbert(
                    wh_angles[:, 0], trial_whisking["fps"], f_start=4.0, f_stop=12.0
                )
                if wh_time[0] < tt[-1]:
                    trialHLFP = interp_HLFP(wh_time)
                else:  # !!!!!!!!!!! CET EXCEPTION A ETE MISE POUR LE RAT 4 MAIS CE N'EST PAS TRES NORMAL IL DOIT Y AVOIR UN PB SUR LE CALCUL DES TEMPS POUR CE RAT, A VERIFIER
                    trialHLFP = np.empty(wh_time.size)
                    trialHLFP[:] = np.nan

                trial_serie = pd.Series()
                trial_serie["rat"] = rat_number
                trial_serie["session"] = session
                trial_serie["trialnumb"] = trial_numb
                trial_serie["touch_start"] = trial_beha["startcontact"].values[0]
                trial_serie["touch_dur"] = trial_beha["stopcontact"].values[0]
                trial_serie["times"] = wh_time
                trial_serie["HLFP"] = trialHLFP
                trial_serie["Hwhisk"] = Hwhisk
                trial_serie["success"] = trial_beha["correctness"].values[0]
                trial_serie["texture"] = trial_beha["texture"].values[0]
                trial_serie["rewardtime"] = trial_beha["rewardtime"].values[0]
                all_trial_series.append(trial_serie)

    full_df = pd.concat(all_trial_series, axis=1).T
    full_df.to_pickle("DataFrame_TrialHilbert_whisker1.out")


if __name__ == "__main__":
    main()

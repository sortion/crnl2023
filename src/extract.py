"""
A damn small utility to extract signal windows around target timestamps.
"""

from typing import List, Union

import numpy as np
from debugprint import Debug

debug = Debug("extract")


def extract_all_around(
    signal: List[float],
    signal_times: List[float],
    target_times: List[float],
    width: Union[int, tuple[int]] = 10,
):
    """
    Extracts windows around target timestamps.

    :param signal:
    :param signal_times: timestamps of each signal data points
    :param target_times: target timestamps around which signal windows will be extracted
    :param width: window width(s), as number of samples (may be a couple of -delta_t + other_delta_t)
    :return: windows of signal around of length 2*width with width samples around each target timestamp
    :rtype: np.array[np.array[float]] (shape: (len(target_times), 2*width or width[0] + width[1]))
    """
    windows = []
    for target_time in target_times:
        window = extract_around(signal, signal_times, target_time, width)
        if window is not None:
            windows.append(window)
    return np.array(windows)


def extract_around(
    signal: List[float],
    signal_times: List[float],
    target_timestamp: float,
    width: Union[int, tuple[int]] = 10,
):
    """
    Extracts a window around a target timestamp.

    :param signal:
    :param signal_times: timestamps of each signal data points
    :param target_timestamp: target timestamp around which signal window will be extracted
    :param width: window width, in samples
    :return: window of signal around of length 2*width with width samples around target timestamp or None if window is not applicable
    """
    if not isinstance(width, tuple):
        width = (width, width)
    closest_frame = np.argmin(np.abs(signal_times - target_timestamp))
    if closest_frame == -1:
        return None
    if closest_frame + width[1] >= len(signal):
        return None
    if closest_frame - width[0] < 0:
        return None
    return signal[closest_frame - width[0] : closest_frame + width[1]]

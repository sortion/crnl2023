"""
Plot a Morlet wavelet.
@see https://psymbio.github.io/posts/morlet/

"""

import matplotlib.pyplot as plt
import numpy as np

plt.rcParams["figure.figsize"] = (8, 3)


fwhm = 0.5
time = np.arange(-1, 1, 0.001)
freq = 2 * np.pi
gaussian = np.exp((-4 * np.log(2)) * (time**2) / fwhm**2)

sin_wave = np.cos(2 * np.pi * freq * time) - 0.1 * np.sin(2 * np.pi * freq * time)
wavelet = sin_wave * gaussian

plt.plot(time, wavelet)
plt.xlabel("Time")
plt.ylabel("Amplitude")
plt.title("Morlet Wavelet")
plt.tight_layout()
plt.savefig("report/media/plots/morlet_wavelet.pdf")

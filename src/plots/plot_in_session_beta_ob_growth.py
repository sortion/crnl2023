import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

sns.set_theme(style="darkgrid")
mpl.pyplot.style.use(mpl.matplotlib_fname())


def plot_session(df_session, column, title=""):
    g = sns.relplot(
        data=df_session,
        x="trial",
        y=column,
        col="structure",
        kind="line",
        err_style="bars",
        legend=False,
        facet_kws={"sharey": False, "sharex": True},
    )
    plt.suptitle(title)
    g.set_titles("")
    g.set_axis_labels("Trial number", "Amplitude")
    g.tight_layout()


dataframe = pd.read_csv("lfp/data/all_measures_2.csv", comment="#")
dataframe["phase_testday"] = dataframe["phase"] + "-" + dataframe["testday"]

measures = ["beta"]
for measure in measures:
    dataframe[f"{measure}_normalized"] = (
        dataframe[measure] / dataframe[f"norm_{measure}"]
    )

df_session = dataframe[dataframe["phase_testday"] == "P1-S1"]
df_session = df_session[df_session["structure"] == "BO"]

plot_session(
    df_session.query("trial < 25"),
    "beta_normalized",
    r"$\beta$ modulation in OB at the beginning of P3 session",
)

plt.savefig("./media/plots/beta_ob_growth_P3-S1.pdf")

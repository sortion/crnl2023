#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generate a plot of phase synchrony between whisking, accross whisking texture touch trials
time is aligned on whisker touch, and data is taken in a window of 2s around touch
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats

from astropy.stats import rayleightest


def extract_windows(df, rat, delta_t=2):
    df_rat = df[df["rat"] == rat]

    # Extract windows of delta_t around touch
    datasets = []
    for i, row in df_rat.iterrows():
        touch_time = row["touch_start"]
        times = row["times"]
        H_whisking = row["Hwhisk"]
        H_lfp = row["HLFP"]
        mask = (times > touch_time - delta_t / 2) & (times < touch_time + delta_t / 2)
        datasets.append(
            pd.DataFrame(
                {
                    "Hwhisk": H_whisking[mask],
                    "HLFP": H_lfp[mask],
                }
            )
        )
    return pd.concat(datasets, ignore_index=True)


def main():
    # Load Grion et al. 2016 data (dataframe formatted in fill_db_Grion_py3.py)
    df = pd.read_pickle("data/1536795/DataFrame_TrialHilbert_whisker1.out")

    rat = 3

    delta_t = 2
    df_windows = extract_windows(df, rat, delta_t=delta_t)

    # Compute phase synchrony
    rayleigh_pvalues = rayleightest(df_windows["Hwhisk"])
    rayleigh_zscore = scipy.srats.norm.ppf(rayleigh_pvalues)
    time_range = np.linspace(-delta_t, delta_t, len(rayleigh_zscore))
    plt.figure()
    plt.plot(time_range, rayleigh_zscore)
    plt.xlabel("Time [s]")
    plt.ylabel("Z-score")
    plt.title(
        "Rayleigh test z-score for {}'s whisking \
              phase around texture touch".format(
            rat
        )
    )
    plt.show()


if __name__ == "__main__":
    main()

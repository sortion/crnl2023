"""
Extract stacked signals.
"""

import sys

import pandas as pd

sys.path.append("lfp")
from dataio import dataio, rk_session

run_key = rk_session("Rtbsi28", "P1", "LC1")

channel_names = dataio.get_all_channel_names(run_key)
data = {}

for channel_name in channel_names:
    signal, _, _ = dataio.get_signal(run_key, channel_name=channel_name)
    data[channel_name] = signal

df = pd.DataFrame(data)

df.to_pickle("data/Rtbsi28_P1_LC1_signals_df.out")

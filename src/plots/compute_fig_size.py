# ref. https://scipy-cookbook.readthedocs.io/items/Matplotlib_LaTeX_Examples.html
from math import sqrt

fig_width_pt = 455.24411
inches_per_pt = 1.0 / 72.27
golden_mean = (sqrt(5) - 1.0) / 2.0
fig_width = fig_width_pt * inches_per_pt
fig_height = fig_width * golden_mean
fig_size = [fig_width, fig_height]
print("figure.figsize:", fig_size)

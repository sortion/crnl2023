import sys

import xarray as xr

sys.path.append("lfp")
from precompute_tfmap import load_tfmap

data = load_tfmap("gammaNotch_PPend", pattern="Rtbsi16", struct="BO")
data = xr.concat([d.stack(rk_PP=["run_key", "PPindex"]) for d in data], "rk_PP")

avgpow_max = data.mean(["rk_PP"], skipna=True).max()
data.name = f"tfmap_Rtbsi11_AP"
times = tuple(data.coords["Time"].values[[0, -1]])
freqs = tuple(data.coords["Freq"].values[[0, -1]])
struct_df = data.mean(["rk_PP"], skipna=True).to_dataframe()

struct_df.to_pickle("data/tfmap_Rtbsi16_BO.pkl")

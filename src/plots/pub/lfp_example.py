import matplotlib.pyplot as plt
import pandas as pd

max_samples = 1000

df = pd.read_pickle("data/Rtbsi38_P1_S1_signals_df.out")

plt.plot(df["Hipp"][:max_samples], color="black")
plt.axis("off")
plt.savefig("media/pictures/lfp/lfp_example.svg", dpi=300)

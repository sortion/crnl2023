import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

sessions = [
    "P1-S1",
    "P1-S2",
    "P1-LC0",
    "P1-LC1",
    "P1-LC2",
    "P1-LC3",
    "P2-S1",
    "P2-LC0",
    "P2-LC1",
    "P2-LC2",
    "P2-LC3",
    "P3-S1",
    "P3-LC0",
    "P3-LC1",
    "P3-LC2",
    "P3-LC3",
    "P1-T1",
    "P1-T2",
]


def preprocess(csv_file):
    df = pd.read_csv(csv_file, comment="#")
    # Add a column for phase and testday
    df["phase_testday"] = df["phase"] + "-" + df["testday"]
    # Remove all row with testday in P1 not S1 or S2, keep all others
    df = df[df["phase_testday"].isin(sessions)]
    return df


def preprocess_norm(csv_file):
    df = preprocess(csv_file)
    df["normalized_measure"] = df["beta"] / df["norm_beta"]
    df = df.groupby(["rat", "structure", "phase_testday", "phase", "testday"]).mean()
    return df


def plot_norm(df, title="meas"):
    sns.set_theme(style="darkgrid")
    mpl.pyplot.style.use(mpl.matplotlib_fname())
    g = sns.relplot(
        data=df,
        x="phase_testday",
        y="normalized_measure",
        col="structure",
        kind="line",
        col_wrap=3,
        err_style="bars",
        legend=False,
        facet_kws={"sharey": False, "sharex": True},
    )
    for struct, ax in g.axes_dict.items():
        # Rotate xticks
        for label in ax.get_xticklabels():
            label.set_rotation(45)
        # Add a label for each structure
        ax.text(0.8, 0.85, struct, transform=ax.transAxes, fontweight="bold")
        # Add lightweight color area for each phase
        phase2_index = sessions.index("P2-S1")
        phase3_index = sessions.index("P3-S1")
        phase4_index = sessions.index("P1-T1")
        ax.axvspan(phase2_index - 0.5, phase3_index - 0.5, alpha=0.1, color="red")
        ax.axvspan(phase3_index - 0.5, phase4_index - 0.5, alpha=0.1, color="green")
    plt.xticks(rotation=45)
    plt.suptitle(title)
    g.set_titles("")
    g.set_axis_labels("Session", "Amplitude")
    g.tight_layout()


plot_norm(preprocess_norm("lfp/data/all_measures_2.csv"), title="beta")
plt.show()
# plt.savefig("media/plots/beta_amplitude_modulation.pdf")

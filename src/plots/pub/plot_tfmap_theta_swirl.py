import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd

struct_df = pd.read_pickle("data/tfmap_theta_Rtbsi35_Hipp.pkl")

fig, ax = plt.subplots()
img = ax.imshow(
    struct_df.unstack("Time"),
    origin="lower",
    aspect="auto",
    extent=(-2.0, 3.0, 2.0, 19.5),
)
time_window = [-0.4, -0.2]
freq_window = [7.5, 10]

rect = mpl.patches.Rectangle(
    (time_window[0], freq_window[0]),
    time_window[1] - time_window[0],
    freq_window[1] - freq_window[0],
    linewidth=1,
    edgecolor="red",
    facecolor="none",
)
ax.add_patch(rect)
time_window = [-0.1, 0.1]
freq_window = [6.5, 7.5]
rect = mpl.patches.Rectangle(
    (time_window[0], freq_window[0]),
    time_window[1] - time_window[0],
    freq_window[1] - freq_window[0],
    linewidth=1,
    edgecolor="orange",
    facecolor="none",
)
ax.add_patch(rect)

plt.title(r"TF-map for Rtbsi35 Hipp. $\theta$")
plt.xlabel("Time (s)")
plt.ylabel("Frequency (Hz)")

plt.savefig("media/plots/theta_tfmap_Rtbsi35_Hipp_sq.pdf")

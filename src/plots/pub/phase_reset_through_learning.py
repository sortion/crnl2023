"""
Plot evolution of phase reset Z-score peak through learning.
"""
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

sessions = [
    "P1-S1",
    "P1-S2",
    "P1-LC0",
    "P1-LC1",
    "P1-LC2",
    "P1-LC3",
    "P2-S1",
    "P2-LC0",
    "P2-LC1",
    "P2-LC2",
    "P2-LC3",
    "P3-S1",
    "P3-LC0",
    "P3-LC1",
    "P3-LC2",
    "P3-LC3",
    "P1-T1",
    "P1-T2",
]


sns.set_theme(style="darkgrid")
mpl.pyplot.style.use(mpl.matplotlib_fname())

# Load data
df = pd.read_csv("lfp/data/phase_reset/peak_zscore_sessions.csv")
df["phase_testday"] = df["phase"] + "-" + df["testday"]

# Plot
g = sns.relplot(
    data=df,
    x="phase_testday",
    y="peak",
    kind="line",
    err_style="bars",
    ci=0.95,
)
for ax in g.axes.flat:
    # Add lightweight color area for each phase
    phase2_index = sessions.index("P2-S1")
    phase3_index = sessions.index("P3-S1")
    phase4_index = sessions.index("P1-T1")
    ax.axvspan(phase2_index - 0.5, phase3_index - 0.5, alpha=0.1, color="red")
    ax.axvspan(phase3_index - 0.5, phase4_index - 0.5, alpha=0.1, color="green")
    plt.xticks(rotation=45)
    plt.suptitle("Sniffing phase reset evolution through learning")
    g.set_titles("")
    g.set_axis_labels("Session", "Z-score peak")
    g.tight_layout()

plt.savefig("media/figures/sniff_phase_reset_through_learning.pdf")

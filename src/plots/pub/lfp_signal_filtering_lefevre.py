"""
Generate a plot for report showing the process of filtering LFP signal,
computing phase with Hilbert transform
"""

import matplotlib.pyplot as plt
import pandas as pd

# import sys

# sys.path.append("lfp")
# from dataio import rk_session, dataio
# from tools import compute_filtered_sig, compute_hilbert_envelope

# sys.path.append("src")
# from extract import extract_around

# RAT = "Rtbsi11"
# PHASE, TESTDAY = "P1", "S1"

# PP_df = dataio.PP_df.reset_index()

# Time = PP_df.loc[
#     (PP_df["Animal"] == RAT)
#     & (PP_df["Phase"] == PHASE)
#     & (PP_df["Testday"] == TESTDAY),
#     "Time",
# ].values[3]

# # Load data
# run_key = rk_session(RAT, PHASE, TESTDAY)

# LFP, LFP_sr, LFP_tstart = dataio.get_signal(run_key, channel_name="Hipp")
# LFP_times = np.arange(LFP.shape[0]) / LFP_sr + LFP_tstart

# LFP_filtered, LFP_filtered_sr = compute_filtered_sig(LFP, LFP_sr, 5, 15)
# LFP_filtered_times = np.arange(LFP_filtered.shape[0]) / LFP_filtered_sr + LFP_tstart

# LFP_hilbert_times, _, LFP_hilbert, LFP_hilbert_sr = compute_hilbert_envelope(
#     LFP, LFP_sr, 5, 15, t_start=LFP_tstart
# )

# # Extract around PPstart
# LFP = extract_around(LFP, LFP_times, Time, width=int(2 * LFP_sr))
# LFP_times = np.linspace(-2, 2, LFP.shape[0])

# LFP_filtered = extract_around(
#     LFP_filtered, LFP_filtered_times, Time, width=int(2 * LFP_filtered_sr)
# )
# LFP_filtered_times = np.linspace(-2, 2, LFP_filtered.shape[0])

# LFP_hilbert = extract_around(
#     LFP_hilbert, LFP_hilbert_times, Time, width=int(2 * LFP_hilbert_sr)
# )
# LFP_hilbert_times = np.linspace(-2, 2, LFP_hilbert.shape[0])

# pd.Series(
#     {
#         "LFP": LFP,
#         "LFP_times": LFP_times,
#         "LFP_filtered": LFP_filtered,
#         "LFP_filtered_times": LFP_filtered_times,
#         "LFP_hilbert": LFP_hilbert,
#         "LFP_hilbert_times": LFP_hilbert_times,
#     }
# ).to_pickle("data/lfp_signal_filtering_lefevre.pkl")

ds = pd.read_pickle("data/lfp_signal_filtering_lefevre.pkl")

plt.figure()
fig, axs = plt.subplots(3, 1, sharex=True)
axs[0].axvline(x=0, color="black", linestyle="--")
axs[0].plot(ds["LFP_times"], ds["LFP"])
axs[0].set_ylabel("LFP")
# Add vertical lines for whisker touch
axs[1].axvline(x=0, color="black", linestyle="--")
axs[1].plot(ds["LFP_filtered_times"], ds["LFP_filtered"])
axs[1].set_ylabel("filtered")
axs[2].axvline(x=0, color="black", linestyle="--")
axs[2].plot(ds["LFP_hilbert_times"], ds["LFP_hilbert"])
axs[2].set_ylabel("phase")
axs[2].set_xlabel("Time (s)")
fig.align_labels()
plt.suptitle("Filtered LFP phase around PPstart")
plt.tight_layout()
plt.savefig("media/plots/lfp_signal_filtering_lefevre.pdf")

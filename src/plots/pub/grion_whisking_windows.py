#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot Rayleigh test results for whisking windows, Grion et al. data
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import typer
from scipy import stats as sstats

from astropy import stats as astats


def phase_synchronisation(df_rows):
    envelope_array = np.array(
        [row["whisking_hilbert"] for _, row in df_rows.iterrows()]
    )
    # Mask out NaNs
    envelope_array = np.ma.masked_invalid(envelope_array)
    rayleigh_pvalues = astats.rayleightest(envelope_array, axis=0)
    rayleigh_zscore = sstats.norm.ppf(rayleigh_pvalues)
    return rayleigh_zscore


def plot(df, zscores, RAT):
    plt.figure()
    plt.xlabel("Time [s]")
    plt.plot(df["whisking_hilbert_times"][0], zscores)
    plt.ylabel("Z-score")
    plt.title("Rayleigh test z-score for {}'s whisking (Grion et al. 2016)".format(RAT))
    plt.savefig("notebooks/media/plots/grion_whisking_windows_r{}.png".format(RAT))


def cli(rat: int = 6):
    DATA_FILE = f"./data/grion_whisking_windows_r{rat}_df.out"
    df = pd.read_pickle(DATA_FILE)
    zscores = phase_synchronisation(df)
    plot(df, zscores, rat)


def main():
    typer.run(cli)


if __name__ == "__main__":
    main()

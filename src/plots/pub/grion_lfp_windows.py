#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot Rayleigh test results for LFP windows, Grion et al. data
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import typer
from scipy import stats as sstats

from astropy import stats as astats

app = typer.Typer(
    pretty_exceptions_show_locals=False
)  # Avoid printing local variables in tracebacks


def phase_synchronisation(df_rows, session: int = 1):
    df_subset = (
        df_rows[df_rows["session"] == session] if session is not None else df_rows
    )
    envelope_array = np.array(df_subset["LFP_hilbert"].tolist())
    rayleigh_pvalues = astats.rayleightest(envelope_array, axis=0)
    rayleigh_zscore = sstats.norm.ppf(rayleigh_pvalues)
    return rayleigh_zscore


def plot(df, zscores, RAT, session=None):
    plt.figure()
    plt.xlabel("Time [s]")
    plt.plot(df["LFP_hilbert_times"][0], zscores)
    plt.ylabel("Z-score")
    plt.title(
        "Rayleigh test z-score for rat {} LFP {} (Grion et al. 2016)".format(
            RAT, "session {}".format(session) if session is not None else "all sessions"
        )
    )
    plt.savefig(
        "notebooks/media/plots/grion_lfp_windows_r{}_{}.png".format(
            RAT, f"s{session}" if session is not None else "All"
        )
    )


def plot_mixed(df, df_rayleigh, rat, output_file="/tmp/test.png"):
    plt.figure()
    times = df["LFP_hilbert_times"][0]
    for _, row in df_rayleigh.iterrows():
        zscore = row["zscores"]
        if len(zscore) != len(times):
            continue
        plt.plot(times, zscore, label=row["session"])
    plt.xlabel("Time [s]")
    plt.ylabel("Z-score")
    plt.title("Rayleigh test z-score for rat {} LFP (Grion et al. 2016)".format(rat))
    plt.savefig(output_file)


@app.command()
def cli(
    rat: int = 3,
    input_file: str = None,
    by_session: bool = False,
    mixed: bool = False,
    output_file: str = None,
):
    if input_file is None:
        input_file = f"./data/grion_lfp_windows_r{rat}_df.out"
    df = pd.read_pickle(input_file)
    if mixed:
        rayleigh_zscores = []
        for session in df["session"].unique():
            zscores = phase_synchronisation(df, session=session)
            rayleigh_zscores.append(pd.Series({"session": session, "zscores": zscores}))
        df_rayleigh = pd.concat(rayleigh_zscores, axis=1).T
        if output_file is None:
            output_file = f"notebooks/media/plots/grion_lfp_windows_rayleigh_r{rat}.png"
        plot_mixed(df, df_rayleigh, rat, output_file=output_file)
    elif by_session:
        for session in df["session"].unique():
            zscores = phase_synchronisation(df, session=session)
            plot(df, zscores, rat, session=session)
    else:
        zscores = phase_synchronisation(df)
        plot(df, zscores, rat)


def main():
    app()


if __name__ == "__main__":
    main()

#!/usr/bin/env python3
"""

"""

import os

import matplotlib.pyplot as plt
import scipy.stats as st
import xarray as xr

from astropy.stats import rayleightest

RAT = "Rtbsi11"
ENVELOPE_DATAFILE = os.path.join("./data/", "hilbert_env_{}.nc".format(RAT))
# ENVELOPE_DATAFILE = os.path.join(configuration.precomputedir, "phases", "hilbert_env_{}_LC2_.nc".format(RAT))
# ENVELOPE_DATAFILE = os.path.join(configuration.precomputedir, "phases", "hilbert_env_{}_LC2_1-15Hz.nc".format(RAT))


def main():
    # Load data
    envelope_dataset = xr.open_dataset(ENVELOPE_DATAFILE)
    envelope_array = envelope_dataset.to_array()[0]
    # Apply rayleigh test to each time point accross windows
    rayleigh_pvalues = rayleightest(envelope_array, axis=0)
    # Convert pvalues to zscores (Max's way)
    rayleigh_zscores = st.norm.ppf(rayleigh_pvalues)
    # Plot
    plt.figure()
    plt.plot(envelope_array["time"], rayleigh_zscores)
    plt.xlabel("Time (s)")
    plt.ylabel("Z-score")
    plt.title("Rayleigh test for {}'s sniffing phase reset".format(RAT))
    plt.savefig(
        os.path.join(
            "media/plots",
            "rayleigh_zscore_sniff_{}_1-15Hz.pdf".format(RAT),
        )
    )


if __name__ == "__main__":
    main()

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

sessions = [
    "P1-S1",
    "P1-S2",
    "P1-LC0",
    "P1-LC1",
    "P1-LC2",
    "P1-LC3",
    "P2-S1",
    "P2-LC0",
    "P2-LC1",
    "P2-LC2",
    "P2-LC3",
    "P3-S1",
    "P3-LC0",
    "P3-LC1",
    "P3-LC2",
    "P3-LC3",
    "P1-T1",
    "P1-T2",
]


data = pd.read_excel("data/DataFrame_PP_base2.xls", index_col=[0, 1, 2])
data = data.reset_index()
# Compute performance for each session, rat per rat
series = {"rat": [], "phase": [], "testday": [], "performance": []}
for rat in data["Animal"].unique():
    for session in sessions:
        phase = session.split("-")[0]
        testday = session.split("-")[1]
        # Count where Sucess is 'reussite'
        success = data[
            (data["Animal"] == rat)
            & (data["Phase"] == phase)
            & (data["Testday"] == testday)
            & (data["Success"] == "reussite")
        ].count()
        # Count where Sucess is 'echec'
        failure = data[
            (data["Animal"] == rat)
            & (data["Phase"] == phase)
            & (data["Testday"] == testday)
            & (data["Success"] != "reussite")
        ].count()
        performance = success / (success + failure)
        series["rat"].append(rat)
        series["phase"].append(phase)
        series["testday"].append(testday)
        series["performance"].append(performance["Success"])
df = pd.DataFrame(series)
df["phase_testday"] = df["phase"] + "-" + df["testday"]


sns.set_theme(style="darkgrid")
mpl.pyplot.style.use(mpl.matplotlib_fname())

g = sns.relplot(
    data=df,
    x="phase_testday",
    y="performance",
    kind="line",
    err_style="bars",
    legend=False,
    facet_kws={"sharey": False, "sharex": True},
)
# Rotate xticks
ax = plt.gca()
for label in ax.get_xticklabels():
    label.set_rotation(45)
# Add lightweight color area for each phase
phase2_index = sessions.index("P2-S1")
phase3_index = sessions.index("P3-S1")
phase4_index = sessions.index("P1-T1")
ax.axvspan(phase2_index - 0.5, phase3_index - 0.5, alpha=0.1, color="red")
ax.axvspan(phase3_index - 0.5, phase4_index - 0.5, alpha=0.1, color="green")
plt.xticks(rotation=45)
plt.suptitle("Performance of rats in each session")
g.set_titles("")
g.set_axis_labels("Session", "Performance")
g.tight_layout()
plt.savefig("media/plots/rat_performance.pdf")

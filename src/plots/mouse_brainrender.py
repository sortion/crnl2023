import brainrender
from brainrender import Scene
from myterial import blue_grey, green, salmon

brainrender.settings.SHOW_AXES = False

# Explicitly initiliase a scene with the screenshot folder set
# If the screenshot folder is not set, by default screenshots
# Will save to the current working directory
screenshot_folder = "./notebooks/media/images/brainrender/screenshots"
scene = Scene(
    title="Mouse brain",
    inset=True,
    screenshots_folder=screenshot_folder,
)

# Add some actors to the scene
PA = scene.add_brain_region("PA", alpha=0.2, silhouette=False, color=salmon)
MOB = scene.add_brain_region("MOB", alpha=0.4, silhouette=False, color=[50, 2, 155])
CB = scene.add_brain_region("CB", alpha=0.1, silhouette=False, color=green)
STR = scene.add_brain_region("STR", alpha=0.1, silhouette=False, color=blue_grey)
HIP = scene.add_brain_region("HIP", alpha=0.4, silhouette=False, color=blue_grey)

scene.add_label(MOB, "OB")
scene.add_label(PA, "Pir")
scene.add_label(CB, "Cereb")
scene.add_label(STR, "Stri")
scene.add_label(HIP, "Hipp")


# scene.slice("sagittal")

# Set up a camera. Can use string, such as "sagittal".
# During render runtime, press "c" to print the current camera parameters.
camera = {
    "pos": (8777, 1878, 44032),
    "viewup": (0, -1, 0),
    "clippingRange": (24852, 54844),
    "focalPoint": (7718, 4290, -3507),
    "distance": 40610,
}
zoom = 2

# If you only want a screenshot and don't want to move the camera
# around the scene, set interactive to False.
scene.render(
    interactive=False,
    camera=camera,
    zoom=zoom,
)

# Set the scale, which will be used for screenshot resolution.
# Any value > 1 increases resolution, the default is in brainrender.settings.
# It is easiest integer scales (non-integer can cause crashes).
scale = 2

# Take a screenshot - passing no name uses current time
# Screenshots can be also created during runtime by pressing "s"
scene.screenshot(name="mouse_brainrender", scale=scale)

# scene.close()

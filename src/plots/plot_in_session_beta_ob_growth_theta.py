import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

sns.set_theme(style="darkgrid")
mpl.pyplot.style.use(mpl.matplotlib_fname())

sessions = [
    "P1-S1",
    "P1-S2",
    "P1-LC0",
    "P1-LC1",
    "P1-LC2",
    "P1-LC3",
    "P2-S1",
    "P2-LC0",
    "P2-LC1",
    "P2-LC2",
    "P2-LC3",
    "P3-S1",
    "P3-LC0",
    "P3-LC1",
    "P3-LC2",
    "P3-LC3",
    "P1-T1",
    "P1-T2",
]
measures = ["upper_theta", "lower_theta", "beta", "gamma40", "gamma55", "gamma65"]


def preprocess(csv_file):
    df = pd.read_csv(csv_file, comment="#")
    # Add a column for phase and testday
    df["phase_testday"] = df["phase"] + "-" + df["testday"]
    for measure in measures:
        df[f"{measure}_normalized"] = df[measure] / df[f"norm_{measure}"]
    return df


df = preprocess("lfp/data/all_measures_2.csv")

df = df[df["phase_testday"].isin(sessions)]
sns.set_theme(style="darkgrid")

fig, axs = plt.subplots(1, 2)

struct = "TO"

g = sns.lineplot(
    data=df.query("structure == @struct"),
    x="phase_testday",
    y="upper_theta_normalized",
    err_style="bars",
    legend=False,
    ax=axs[0],
)
g = sns.lineplot(
    data=df.query("structure == @struct"),
    x="phase_testday",
    y="lower_theta_normalized",
    err_style="bars",
    legend=False,
    ax=axs[1],
)
for ax in axs:
    # Rotate xticks
    for label in ax.get_xticklabels():
        label.set_rotation(45)
    # Add a label for each structure
    ax.text(0.8, 0.85, struct, transform=ax.transAxes, fontweight="bold")
    # Add lightweight color area for each phase
    phase2_index = sessions.index("P2-S1")
    phase3_index = sessions.index("P3-S1")
    phase4_index = sessions.index("P1-T1")
    ax.axvspan(phase2_index - 0.5, phase3_index - 0.5, alpha=0.1, color="red")
    ax.axvspan(phase3_index - 0.5, phase4_index - 0.5, alpha=0.1, color="green")
    ax.set_xlabel("Session")
    ax.set_ylabel("Amplitude")
plt.xticks(rotation=45)
plt.tight_layout()
plt.suptitle(r"lower and upper $\theta$ modulation have opposite trends")


plt.savefig("./report/media/plots/theta_different_modulations.pdf")
# plt.show()

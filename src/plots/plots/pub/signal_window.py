"""
Plot stacked signals in a 10 sec window.
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

max_samples = 2000

df = pd.read_pickle("data/Rtbsi38_P1_S1_signals_df.out")

fig, axs = plt.subplots(
    8,
    1,
    sharex=True,
)
time_range = np.arange(0, max_samples) / 1000

axs[0].plot(time_range, df["BO"][:max_samples], color="blue")
axs[0].set_ylabel("OB")
axs[1].plot(time_range, df["PA"][:max_samples], color="orange")
axs[1].set_ylabel("AP")
axs[2].plot(time_range, df["PP"][:max_samples], color="green")
axs[2].set_ylabel("PP")
axs[3].plot(time_range, df["TO"][:max_samples], color="red")
axs[3].set_ylabel("OT")
axs[4].plot(time_range, df["Stri"][:max_samples], color="purple")
axs[4].set_ylabel("Stri")
axs[5].plot(time_range, df["Hipp"][:max_samples], color="brown")
axs[5].set_ylabel("Hipp")
axs[6].plot(time_range, df["Cerv"][:max_samples], color="pink")
axs[6].set_ylabel("Cereb")
axs[7].plot(time_range, df["Respi"][:max_samples], color="gray")
axs[7].set_ylabel("Respi")
fig.align_labels()
for ax in axs:
    # ax.set_xticks([])
    ax.set_yticks([])
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)
    # Rotate y label
    ax.set_ylabel(ax.get_ylabel(), rotation=0)
fig.align_labels()
axs[7].set_xlabel("Time (s)")
plt.suptitle(r"LFP and respiratory signals")
plt.tight_layout()
fig.subplots_adjust(hspace=0)
plt.savefig("report/media/plots/signals_window_Lefèvre.pdf")

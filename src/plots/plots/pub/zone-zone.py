import sys

import matplotlib.pyplot as plt
import pandas as pd

sys.path.append("lfp")
import connect.plots.zone as zone

df = pd.read_csv("lfp/data/wpli/measures.csv")
df["phase_testday"] = df["phase"] + "-" + df["testday"]

zone.plot_zone_triangle(df)
plt.suptitle(f"""WPLI between brain regions for all rats across sessions""")
plt.savefig("report/media/plots/zone-zone_WPLI_all_rats.pdf")

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd

struct_df = pd.read_pickle("data/tfmap_Rtbsi16_BO.pkl")

fig, ax = plt.subplots()
img = ax.imshow(
    struct_df.unstack("Time"),
    origin="lower",
    aspect="auto",
    extent=(-2.0, 3.0, 20.0, 198.0),
)
time_window = [-0.200, 0.200]  # s
freq_window = [50, 70]  # Hz
rect = mpl.patches.Rectangle(
    (time_window[0], freq_window[0]),
    time_window[1] - time_window[0],
    freq_window[1] - freq_window[0],
    linewidth=1,
    edgecolor="r",
    facecolor="none",
)
ax.add_patch(rect)
plt.title("Gamma amplitude measure zone on Rtbsi16's TFmaps in BO")
plt.xlabel("Time (s)")
plt.ylabel("Frequency (Hz)")

plt.savefig("report/media/plots/gamma_amplitude_measure_zone_rtbsi16.pdf")

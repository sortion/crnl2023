#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot Rayleigh test results for LFP windows, Grion et al. data
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import stats as sstats

from astropy import stats as astats


def phase_synchronisation(df_rows):
    envelope_array = np.array([row["LFP_hilbert"] for _, row in df_rows.iterrows()])
    rayleigh_pvalues = astats.rayleightest(envelope_array, axis=0)
    rayleigh_zscore = sstats.norm.ppf(rayleigh_pvalues)
    return rayleigh_zscore


def plot(df, zscores):
    plt.figure()
    plt.xlabel("Time [s]")
    plt.plot(df["LFP_hilbert_times"][0], zscores)
    plt.ylabel("Z-score")
    plt.title("Rayleigh test z-score for all rat LFP (Grion et al. 2016)")
    plt.savefig("notebooks/media/plots/grion_lfp_windows_rAll.png")


def main():
    DATA_FILE = f"./data/grion_windows_2s_5-12Hz_merged.out"
    df = pd.read_pickle(DATA_FILE)
    zscores = phase_synchronisation(df)
    plot(df, zscores)


if __name__ == "__main__":
    main()

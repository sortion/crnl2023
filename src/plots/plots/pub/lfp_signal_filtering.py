"""
Generate a plot for report showing the process of filtering LFP signal,
computing phase with Hilbert transform
"""

import matplotlib.pyplot as plt
import pandas as pd

DATA_FILE = "data/grion_lfp_windows_r3_df.out"
df = pd.read_pickle(DATA_FILE).iloc[0]

plt.figure()
fig, axs = plt.subplots(3, 1, sharex=True)
axs[0].axvline(x=0, color="black", linestyle="--")
axs[0].plot(df["LFP_times"], df["LFP"])
axs[0].set_ylabel("LFP")
# Add vertical lines for whisker touch
axs[1].axvline(x=0, color="black", linestyle="--")
axs[1].plot(df["LFP_filtered_times"], df["LFP_filtered"])
axs[1].set_ylabel("filtered")
axs[2].axvline(x=0, color="black", linestyle="--")
axs[2].plot(df["LFP_hilbert_times"], df["LFP_hilbert"])
axs[2].set_ylabel("phase")
axs[2].set_xlabel("Time (s)")
for ax in axs:
    ax.set_ylabel(ax.get_ylabel(), rotation=0)
fig.align_labels()
plt.suptitle(r"Filtered LFP phase around whisker touch event")
plt.tight_layout()
plt.savefig("report/media/plots/lfp_signal_filtering.pdf")

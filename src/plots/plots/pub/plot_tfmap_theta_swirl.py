import matplotlib.pyplot as plt
import pandas as pd

struct_df = pd.read_pickle("data/tfmap_theta_Rtbsi35_Hipp.pkl")

fig, ax = plt.subplots()
img = ax.imshow(
    struct_df.unstack("Time"),
    origin="lower",
    aspect="auto",
    extent=(-2.0, 3.0, 2.0, 19.5),
)
plt.title("TF-map for Rtbsi35 Hipp Theta")
plt.xlabel("Time (s)")
plt.ylabel("Frequency (Hz)")

plt.savefig("report/media/plots/theta_tfmap_Rtbsi35_Hipp.pdf")

"""
Extract stacked signals.
"""

import sys

import pandas as pd

sys.path.append("lfp")
from dataio import dataio, rk_session

run_key = rk_session("Rtbsi38", "P1", "S1")

channel_names = dataio.get_all_channel_names(run_key)
data = {}

for channel_name in channel_names:
    signal, _, _ = dataio.get_signal(run_key, channel_name=channel_name)
    data[channel_name] = signal

df = pd.DataFrame(data)

df.to_pickle("Rtbsi38_P1_S1_signals_df.out")

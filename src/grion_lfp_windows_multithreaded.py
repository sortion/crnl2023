# ~/Projects/intership/.venv/crnl-clust/bin/python3
# -*- coding: utf8 -*-

"""
Extract raw and filtered signal data from the generated Grion et al. data pandas dataframe.
Signal are stacked as windows around -2s/+2s of each signal around the whisker touch timestamp.
Data is saved as a xarray dataset, only for rat 3, for the moment.
"""

import multiprocessing as mp
from typing import List

import numpy as np
import pandas as pd
import typer
import xarray as xr
from tqdm.auto import tqdm

from extract import extract_around
from tools import compute_filtered_sig, compute_hilbert_envelope


def max_delta_t(grion_df):
    min_delta: float = None
    for _, row in grion_df.iterrows():
        LFP_times = row["LFP_times"]
        whisker_touch_time = row["touch_start"]
        if np.any(np.isnan(LFP_times)):
            continue
        min_time = LFP_times[0]
        max_time = LFP_times[-1]
        max_delta_time = min(
            whisker_touch_time - min_time, max_time - whisker_touch_time
        )
        if min_delta is None:
            min_delta = max_delta_time
        elif min_delta > max_delta_time:
            min_delta = max_delta_time
    return min_delta


def construct_windows_around_parallel(grion_df, f_min, f_max):
    def worker(grion_df, delta_t, f_min, f_max, queue):
        dataset = construct_windows_around(grion_df, delta_t, f_min, f_max)
        queue.put(dataset)

    # Set delta_t to maximal values,
    # when all windows fits in the available data
    delta_t = max_delta_t(grion_df)

    # Create a queue to share data between processes
    queue = mp.Queue()
    # Create a process for each CPU
    processes = []
    for i in range(mp.cpu_count()):
        df_slice = grion_df.iloc[i :: mp.cpu_count()]
        p = mp.Process(target=worker, args=(grion_df, delta_t, f_min, f_max, queue))
        p.start()
        processes.append(p)
    # Wait for all processes to finish, and add a tqdm progress bar
    for p in tqdm(processes):
        p.join()
    # Get results from queue
    results = [queue.get() for p in processes]
    # Concatenate all results
    dataset = xr.concat(results, dim="trial")
    return dataset


def construct_windows_around(grion_df, delta_t, f_min, f_max):
    """

    :return: Signal windows around touch start dataset
    :rtype: xarray.DataSet
    """

    windows: List[xr.DataArray] = []
    # grion_df = grion_df.iloc[:10]
    for _, row in grion_df.iterrows():
        LFP = row["LFP"]
        LFP_times = row["LFP_times"]
        LFP_sr = 2000
        whisker_touch_time = row["touch_start"]
        # Avoid NaNs
        if np.any(np.isnan(LFP_times)):
            continue
        # Extract raw LFP signal window
        # LFP_window = extract_around(
        #     LFP, LFP_times, whisker_touch_time, int(LFP_sr * delta_t)
        # )
        # Compute filtered signal
        LFP_filtered, LFP_filtered_sr = compute_filtered_sig(LFP, LFP_sr, f_min, f_max)
        LFP_filtered_times = np.linspace(LFP_times[0], LFP_times[-1], len(LFP_filtered))
        # Compute hilbert envelope
        env_times, env_ampl, env_phase, env_sampling_rate = compute_hilbert_envelope(
            LFP, LFP_sr, f_min, f_max
        )
        LFP_filtered_window = extract_around(
            LFP_filtered,
            LFP_filtered_times,
            whisker_touch_time,
            int(delta_t * LFP_filtered_sr),
        )
        LFP_hilbert_window = extract_around(
            env_phase, env_times, whisker_touch_time, int(delta_t * env_sampling_rate)
        )
        if (
            # LFP_window is not None
            LFP_filtered_window is not None
            and LFP_hilbert_window is not None
        ):
            # datarray_LFP_window = xr.DataArray(
            #     LFP_window,
            #     coords=[("time", np.linspace(-delta_t, delta_t, len(LFP_window)))],
            #     attrs={"type": "raw"},
            # )
            # windows.append(datarray_LFP_window)
            datarray_LFP_filtered_window = xr.DataArray(
                LFP_filtered_window,
                coords=[
                    ("time", np.linspace(-delta_t, delta_t, len(LFP_filtered_window)))
                ],
                attrs={"type": "filtered"},
            )
            windows.append(datarray_LFP_filtered_window)
            dataarray_LFP_hilbert_window = xr.DataArray(
                LFP_hilbert_window,
                coords=[
                    ("time", np.linspace(-delta_t, delta_t, len(LFP_hilbert_window)))
                ],
                attrs={"type": "hilbert"},
            )
            windows.append(dataarray_LFP_hilbert_window)
    if windows == []:
        raise ValueError("No windows found")
    dataset = xr.concat(windows, dim="trial")
    return dataset


def cli(rat: int = 3):
    PICKLE_FILE = "./data/grion_db.out"
    F_MIN = 5
    F_MAX = 12
    grion_df = pd.read_pickle(PICKLE_FILE)
    grion_df_filtered = grion_df[grion_df["rat"] == rat]
    dataset: xr.DataSet = construct_windows_around_parallel(
        grion_df_filtered, F_MIN, F_MAX
    )
    dataset.to_netcdf(f"./data/grion_windows_flp_R{rat}.nc")


def main():
    typer.run(cli)


if __name__ == "__main__":
    main()

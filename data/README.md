# Data source

## Whisking and LFP data

- `1536795.zip` the data from Grion *et al.* (2016) ^[Link to data share: <http://figshare.com/s/99b31b8a567f11e5b81d06ec4bbcf141>]

## Sniffing and LFP data

Data from Lefevre *et al.* 2016 is available on CRNL servers.

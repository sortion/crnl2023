"""
A workflow to generate phase reset plots for FLP data.
Data comes from Grion et al. dataframe extract.

Frequency band is configurable in config/config.yml file.
"""

import os
import pandas as pd
import numpy as np

configfile: "workflow/config/config.yml"

INPUT_DATAFRAME = config["grion_dataframe"]
OUTPUT_PLOTS_DIR = config["output_plots"]
WORK_DIR = config["work_dir"]
RATS = [3, 4, 5, 6]

def get_sessions(rat):
    df = pd.read_pickle(INPUT_DATAFRAME)
    return (
        df[df["rat"] == rat]["session"].unique()
    )


rule all:
    input:
        expand(os.path.join(OUTPUT_PLOTS_DIR, \
            "grion_lfp_phase_reset_bysession_r{R}_{FMIN}-{FMAX}Hz.png"), \
            R=RATS, FMIN=config["analyzis"]["lfp"]["f_min"], \
            FMAX=config["analyzis"]["lfp"]["f_max"])

rule extract_windows:
    conda: "../../envs/snake_env.yml"
    input:
        df=INPUT_DATAFRAME
    output:
        df=os.path.join(WORK_DIR, "grion_lfp_windows_r{R}_df.out")
    shell:
        """
        python3 src/grion_lfp_windows.py --input-file {input.df} \
            --output-file {output.df} --rat {wildcards.R} \
            --f-min {config[analyzis][lfp][f_min]} --f-max {config[analyzis][lfp][f_max]}
        """

rule plot_phase_reset:
    conda: "../../envs/snake_env.yml"
    input:
        df=os.path.join(WORK_DIR, "grion_lfp_windows_r{R}_df.out")
    output:
        plot=OUTPUT_PLOTS_DIR + \
                "/grion_lfp_phase_reset_bysession_r{R}_" + \
                str(config["analyzis"]["lfp"]["f_min"]) + "-" + str(config["analyzis"]["lfp"]["f_max"]) + "Hz.png"
    shell:
        """
        python3 src/plots/grion_lfp_windows.py --input-file {input.df} \
            --output-file {output.plot} --rat {wildcards.R} --mixed
        """

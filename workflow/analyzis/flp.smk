"""
Generate LFP windows rayleigh plots for each selected rats.
"""
RATS = [3, 4, 5, 6]

rule all:
    input:
        expand('data/grion_lfp_windows_r{R}_df.out', R=RATS),
        expand('notebooks/media/plots/grion_lfp_windows_r{R}.png', R=RATS)

rule extract_windows:
    conda: '../../envs/snake_env.yml'
    input: 'data/grion_db.out'
    output: 'data/grion_lfp_windows_r{R}_df.out'
    shell:
        """
        python3 src/grion_lfp_windows.py --rat {wildcards.R}
        """

rule generate_plot:
    conda: '../../envs/snake_env.yml'
    input: 'data/grion_lfp_windows_r{R}_df.out'
    output: 'notebooks/media/plots/grion_lfp_windows_r{R}.png'
    shell:
        """
        python3 src/plots/grion_lfp_windows.py --rat {wildcards.R}
        """
